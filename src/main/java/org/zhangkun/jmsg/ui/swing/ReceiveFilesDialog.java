package org.zhangkun.jmsg.ui.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.zhangkun.jmsg.bean.DataUnPack;
import org.zhangkun.jmsg.bean.User;
import org.zhangkun.jmsg.core.network.NetConnection;
import org.zhangkun.jmsg.core.network.filetransport.FileTransport;
import org.zhangkun.jmsg.core.network.filetransport.ReceiveFiles;
import org.zhangkun.jmsg.ui.swing.view.FilesTable;
import org.zhangkun.jmsg.util.FileUtil;

public class ReceiveFilesDialog extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2104071198453725159L;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final JPanel contentPanel = new JPanel();
	private DataUnPack dup;
	private FilesTable filesTable;
	private JButton okButton;
	private ReceiveFiles rf;
	private JTextField savePathField;
	private User user;

	/**
	 * Create the dialog.
	 */
	public ReceiveFilesDialog(DataUnPack d, User u) {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});

		dup = d;
		user = u;
		rf = new ReceiveFiles(dup, u);
		setTitle("接收文件");
		setBounds(100, 100, 500, 360);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			filesTable = new FilesTable();
			filesTable.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null,
					null));
			filesTable.init(rf.getFmps(), "接收");
			contentPanel.add(filesTable);
		}
		{
			String userName = user.getName().length() == 0 ? user.getHostName()
					: user.getName();
			JLabel label = new JLabel(userName + "(IP:" + user.getIp() + ")"
					+ " 向您发送文件，请处理：");
			contentPanel.add(label, BorderLayout.NORTH);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.SOUTH);
			panel.setLayout(new BorderLayout(0, 0));
			{
				savePathField = new JTextField();
				panel.add(savePathField);
				savePathField.setColumns(10);
			}
			{
				JButton button = new JButton("选择位置");
				button.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						JFileChooser sf = new JFileChooser(savePathField
								.getText());
						sf.setDialogTitle("保存路径");
						sf.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
						if (sf.showSaveDialog(ReceiveFilesDialog.this) == JFileChooser.APPROVE_OPTION) {
							savePathField.setText(sf.getSelectedFile()
									.getAbsolutePath());
						}

					}
				});
				panel.add(button, BorderLayout.EAST);
			}
			{
				JLabel label = new JLabel("保存位置：");
				panel.add(label, BorderLayout.WEST);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				{
					filesTable.updateAcceptNum();
				}
				{
					JLabel label = new JLabel("已存在文件：");
					buttonPane.add(label);
				}
				{
					JRadioButton leapRadioButton = new JRadioButton("跳过");
					leapRadioButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							setExistsProcess(false);
						}
					});
					buttonGroup.add(leapRadioButton);
					buttonPane.add(leapRadioButton);
				}
				{
					JRadioButton overlayRadioButton = new JRadioButton("覆盖");

					overlayRadioButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							setExistsProcess(true);
						}
					});
					overlayRadioButton.setSelected(true);
					buttonGroup.add(overlayRadioButton);
					buttonPane.add(overlayRadioButton);
				}
			}
			{
				JButton button = new JButton("对话");
				button.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						MainWindow.instance.openSendDialog(user);
					}
				});
				buttonPane.add(button);
			}
			okButton = new JButton("接收");
			filesTable.setOkButton(okButton);
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					accept();
				}
			});
			{
				JButton cancelButton = new JButton("不接收");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						close();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			okButton.setActionCommand("OK");
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
		}
		setVisible(true);
	}

	private void accept() {
		String path = savePathField.getText();
		if (FileUtil.isExists(path)) {
			if (!FileUtil.isFile(path)) {
				rf.setSavePath(path);
				rf.start();
				this.setVisible(false);
			} else {
				JOptionPane.showMessageDialog(ReceiveFilesDialog.this,
						"您输入的路径是一个文件，请输入一个文件夹！");
			}
		} else {
			if (JOptionPane.showConfirmDialog(ReceiveFilesDialog.this,
					"您输入的路径不存在，您想创建它吗？", "提示", JOptionPane.OK_OPTION
							| JOptionPane.CANCEL_OPTION) == JOptionPane.OK_OPTION) {
				if (FileUtil.createDir(path))
					accept();
				else
					JOptionPane.showMessageDialog(ReceiveFilesDialog.this,
							"对不起，路径创建失败！");
			}
		}
	}

	private void close() {
		if (JOptionPane.showConfirmDialog(ReceiveFilesDialog.this,
				"您确认不接收这些文件吗？", "警告", JOptionPane.OK_OPTION
						| JOptionPane.CANCEL_OPTION) == JOptionPane.OK_OPTION) {
			rf.setStatus(FileTransport.STATUS_USER_CANCEL);
			NetConnection.instance.cancelFileTransport(user.getIp(),
					this.dup.getDataID());
			this.setVisible(false);
			this.dispose();
		} else {
			this.setVisible(true);
		}
	}

	private void setExistsProcess(boolean flag) {
		rf.getFmps();
	}

}
