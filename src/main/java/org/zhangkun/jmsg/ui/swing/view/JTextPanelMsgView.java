package org.zhangkun.jmsg.ui.swing.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.zhangkun.jmsg.bean.User;
import org.zhangkun.jmsg.core.UserManager;
import org.zhangkun.jmsg.ui.swing.listener.MsgViewListener;

public class JTextPanelMsgView extends MsgPanel {

	private JTextArea inputMsg;
	private JTextPane msgPanel;
	private JScrollPane scrollPane_1;

	private JScrollPane scrollPane;
	private JPanel panel_2;
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"HH:MM:ss");

	public JTextPanelMsgView(MsgViewListener msgViewlistener) {
		super(msgViewlistener);
		inputMsg = new JTextArea();
		inputMsg.setDragEnabled(false);
		inputMsg.setDropTarget(null);

		setBorder(new EmptyBorder(5, 5, 5, 5));
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWeights = new double[] { 0.0 };
		gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gbl_contentPanel);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "\u6D88\u606F",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		gbc_panel.gridheight = 1;
		gbc_panel.weightx = 100;
		gbc_panel.weighty = 70;
		add(panel, gbc_panel);
		panel.setLayout(new BorderLayout(0, 0));

		scrollPane_1 = new JScrollPane();
		panel.add(scrollPane_1, BorderLayout.CENTER);
		msgPanel = new JTextPane();
		msgPanel.setEditable(false);
		msgPanel.setContentType("text/html");
		scrollPane_1.setViewportView(msgPanel);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "\u53D1\u9001\u6D88\u606F",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		gbc_panel_1.weightx = 100;
		gbc_panel_1.weighty = 30;
		gbc_panel_1.gridheight = 3;
		add(panel_1, gbc_panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		scrollPane = new JScrollPane(inputMsg);
		panel_1.add(scrollPane);

		inputMsg.setLineWrap(true);
	}

	@Override
	public void appendReceiveMsg(User user, String msg) {
		// TODO Auto-generated method stub
		// this.msg += getMsgHtml(user.getName(), msg, false);
		setDocs("[" + user.getIp() + "] " + user.getName() + " "
				+ dateFormat.format(new Date()), new Color(137, 161, 252),
				false, 12);
		setDocs(msg + "\n", msgPanel.getForeground(), false, 12);
		// System.out.println(this.msg);
		scrollToBottom();
	}

	@Override
	public void appendSendMsg(String msg) {
		// TODO Auto-generated method stub
		String meName = UserManager.me.getName();
		if (meName.length() == 0)
			meName = UserManager.me.getHostName();
		// this.msg += getMsgHtml(meName, msg, true);
		setDocs(meName + " " + dateFormat.format(new Date()), new Color(74,
				181, 128), false, 12);
		setDocs(msg + "\n", msgPanel.getForeground(), false, 12);
		scrollToBottom();
	}

	@Override
	public void appendSystemMsg(String msg) {
		// TODO Auto-generated method stub
		setDocs("系统提示：", Color.GRAY, false, 10);
		setDocs(msg, Color.RED, false, 12);
		scrollToBottom();
	}

	@Override
	public String getInputMsg() {
		// TODO Auto-generated method stub
		return inputMsg.getText();
	}

	@Override
	public void setInputMsg(String msg) {
		// TODO Auto-generated method stub
		inputMsg.setText(msg);
	}

	public void insert(String str, AttributeSet attrSet) {
		Document doc = msgPanel.getDocument();
		str = "\n " + str;
		try {
			doc.insertString(doc.getLength(), str, attrSet);
		} catch (BadLocationException e) {
			System.out.println("BadLocationException:   " + e);
		}
	}

	public void setDocs(String str, Color col, boolean bold, int fontSize) {
		SimpleAttributeSet attrSet = new SimpleAttributeSet();
		StyleConstants.setForeground(attrSet, col);
		// 颜色
		if (bold == true) {
			StyleConstants.setBold(attrSet, true);
		}// 字体类型
		StyleConstants.setFontSize(attrSet, fontSize);
		// 字体大小
		insert(str, attrSet);
	}

	public void setInputMsg(JTextArea inputMsg) {
		this.inputMsg = inputMsg;
	}

	public synchronized void scrollToBottom() {
		// TODO Auto-generated method stub
		JScrollBar jsb = scrollPane_1.getVerticalScrollBar();
		jsb.setValue(jsb.getMaximum());
		scrollPane_1.getViewport().doLayout();
	}

	@Override
	public void setUseHTML(boolean use) {
		// TODO Auto-generated method stub

	}
}
