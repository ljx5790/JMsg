package org.zhangkun.jmsg.ui.swing.view;

import javax.swing.JPanel;

import org.zhangkun.jmsg.bean.User;
import org.zhangkun.jmsg.ui.swing.listener.MsgViewListener;

public abstract class MsgPanel extends JPanel {
	MsgViewListener msgViewListener;

	public MsgPanel(MsgViewListener msgViewListener) {
		this.msgViewListener = msgViewListener;
	}

	public abstract void appendReceiveMsg(User user, String msg);

	public abstract void appendSendMsg(String msg);

	public abstract void appendSystemMsg(String msg);

	public abstract String getInputMsg();

	public abstract void setInputMsg(String msg);

	public abstract void setUseHTML(boolean use);

}
