package org.zhangkun.jmsg.ui.swing.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.l2fprod.common.swing.JLinkButton;

public class ImagePreview extends JPanel implements PropertyChangeListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6743058687218763084L;
	private JFileChooser jfc;
	private Image img;
	private ImgPanel imgPanel = new ImgPanel();
	private JPanel btnPanel = new JPanel();

	private class ImgPanel extends JPanel {

		private Dimension normalSize;

		public Dimension getNormalSize() {
			return normalSize;
		}

		public void setNormalSize(Dimension dim) {
			this.normalSize = dim;
		}

		public void paintComponent(Graphics g) {
			if (img != null) {
				Dimension size = getPreferredSize();
				g.drawImage(img, 0, 0, (int) size.getWidth(),
						(int) size.getHeight(), this);
				// Graphics2D g2d = (Graphics2D) g;
				// g2d.setComposite(AlphaComposite.getInstance(
				// AlphaComposite.SRC_OVER, 0.5f));
				// g2d.setColor(Color.BLACK);
				// Font font = new Font("", Font.PLAIN, 10);
				// g2d.setFont(font);
				// int y = scroll.getHeight() - g.getFontMetrics().getHeight()
				// - 10;
				// g2d.fillRect(0, y, scroll.getWidth(), g.getFontMetrics()
				// .getHeight());
				// g2d.setComposite(AlphaComposite.getInstance(
				// AlphaComposite.SRC_OVER, 1.0f));
				// g2d.setColor(Color.WHITE);
				// String str = getPreferredSize().width + "x"
				// + getPreferredSize().height;
				// int fw = g.getFontMetrics().stringWidth(str);
				// g2d.drawString(str, (scroll.getWidth() - fw) / 2, y
				// + g.getFontMetrics().getAscent());
			}
		}

	}

	JScrollPane scroll;
	JLabel sizeLab = new JLabel();

	public ImagePreview(JFileChooser jfc) {
		this.jfc = jfc;
		Dimension sz = new Dimension(250, 250);
		setPreferredSize(sz);
		setLayout(new BorderLayout());
		scroll = new JScrollPane(imgPanel);
		scroll.setViewportBorder(BorderFactory.createLineBorder(Color.BLUE));
		add(scroll);
		add(btnPanel, BorderLayout.SOUTH);
		JLinkButton btn = new JLinkButton("放大");
		btn.setMargin(new Insets(0, 0, 0, 0));
		btn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Dimension size = imgPanel.getPreferredSize();
				size.height += 10;
				size.width += 10;
				updateShowSize(size);
			}
		});
		btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.X_AXIS));
		btnPanel.add(btn);
		btn = new JLinkButton("缩小");
		Insets insets = new Insets(0, 5, 0, 5);
		btn.setMargin(insets);
		btn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Dimension size = imgPanel.getPreferredSize();
				size.height -= 10;
				size.width -= 10;
				updateShowSize(size);
			}
		});
		btnPanel.add(btn);
		btn = new JLinkButton("比例");
		btn.setMargin(insets);
		btn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Dimension size = scroll.getSize();
				size.width -= 10;
				size.height -= 10;
				updateShowSize(size);
			}
		});
		btnPanel.add(btn);
		btn = new JLinkButton("原图");
		btn.setMargin(insets);
		btn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				updateShowSize(imgPanel.normalSize);
			}
		});
		btnPanel.add(btn);
		btnPanel.add(sizeLab);
	}

	public void propertyChange(PropertyChangeEvent evt) {
		try {
			System.out.println("updating");
			File file = jfc.getSelectedFile();
			updateImage(file);
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		System.out.println("updating1");
	}

	private void updateShowSize(Dimension size) {
		if (size != null) {
			sizeLab.setText(size.width + "x" + size.height);
			imgPanel.setPreferredSize(size);
			scroll.setViewportView(imgPanel);
			getParent().repaint();
		}
	}

	public void updateImage(File file) throws IOException {
		if (file == null) {
			return;
		}
		System.out.println("updateFile" + file.getAbsolutePath());
		img = ImageIO.read(file);
		imgPanel.setNormalSize(new Dimension(img.getWidth(this), img
				.getHeight(this)));
		updateShowSize(imgPanel.normalSize);
	}

}