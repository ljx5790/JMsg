package org.zhangkun.jmsg.ui.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.zhangkun.jmsg.bean.DataPack;
import org.zhangkun.jmsg.bean.DataPackage;
import org.zhangkun.jmsg.bean.Msg;
import org.zhangkun.jmsg.bean.SendMsgInfo;
import org.zhangkun.jmsg.bean.User;
import org.zhangkun.jmsg.core.UserManager;
import org.zhangkun.jmsg.core.listener.RecvMsgListener;
import org.zhangkun.jmsg.core.network.MsgTransport;
import org.zhangkun.jmsg.ui.swing.listener.MsgViewListener;
import org.zhangkun.jmsg.ui.swing.view.MsgPanel;
import org.zhangkun.jmsg.ui.swing.view.SWTMsgView;
import org.zhangkun.jmsg.ui.swing.view.UserTreePanel;

public class GroupMsg extends JFrame implements DropTargetListener,
		RecvMsgListener, MsgViewListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5644328857255766569L;
	private JPanel buttonPane;
	private final JPanel contentPanel = new JPanel();

	private JButton okButton;
	private MsgPanel msgPanel;

	private UserTreePanel userTreePanel;
	private JPanel panel;

	private void close() {
		GroupMsg.this.dispose();
		MainWindow.instance.setGroupMsg(null);
		UserManager.removeUserListener(userTreePanel);
	}

	/**
	 * Create the dialog.
	 */
	public GroupMsg() {
		this.setTitle("群发消息");
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
		// setBounds(100, 100, 563, 439);
		setSize(710, 490);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout());
		userTreePanel = new UserTreePanel("选择好友",
				UserTreePanel.SHOW_MODEL_SELECTION);
		// userTreePanel.setPreferredSize(new Dimension(180, 0));
		getContentPane().add(userTreePanel, BorderLayout.EAST);

		panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		msgPanel = new SWTMsgView(this);
		panel.add(msgPanel, BorderLayout.CENTER);

		buttonPane = new JPanel();
		panel.add(buttonPane, BorderLayout.SOUTH);
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));

		JButton sendFileBtn = new JButton("发送文件");
		sendFileBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new SendFileDialog().getUserTreePanel().selAll();
			}
		});

		buttonPane.add(sendFileBtn);

		okButton = new JButton("发送");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendMsg();
			}
		});

		JButton cancelButton = new JButton("关闭");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		cancelButton.setActionCommand("关闭");
		buttonPane.add(cancelButton);
		okButton.setActionCommand("发送");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		new DropTarget(this, DnDConstants.ACTION_COPY_OR_MOVE, this);
		setVisible(true);

	}

	public UserTreePanel getUserTreePanel() {
		return userTreePanel;
	}

	public void setUserTreePanel(UserTreePanel userTreePanel) {
		this.userTreePanel = userTreePanel;
	}

	@Override
	public void acceptRecvMsg(SendMsgInfo sendMsgInfo, boolean result) {
		// TODO Auto-generated method stub
		if (!result) {
			msgPanel.appendSystemMsg("您发送的消息：‘" + sendMsgInfo.getDp().getMsg()
					+ "’对方可能没有接收，可能是对方已经下线！");
		}
	}

	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dragExit(DropTargetEvent dte) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drop(DropTargetDropEvent dtde) {
		// TODO Auto-generated method stub
		new SendFileDialog().drop(dtde);
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {
		// TODO Auto-generated method stub

	}

	public JPanel getContentPanel() {
		return contentPanel;
	}

	public void addMsg(User user, Msg msg) {
		// TODO Auto-generated method stub
		if (msg.getType() == Msg.MSG_RECEIVE)
			msgPanel.appendReceiveMsg(user, msg.getMsg());
		else if (msg.getType() == Msg.MSG_SEND)
			msgPanel.appendSendMsg(msg.getMsg());
		else if (msg.getType() == Msg.MSG_SYSTEM)
			msgPanel.appendSystemMsg(msg.getMsg());
		else if (msg.getType() == Msg.MSG_SHAKE) {
			msgPanel.appendSystemMsg(user.getName() + " 向您发送了一个窗口抖动。");
			shakeWindow();
		}
	}

	@Override
	public void onSendMsgEvent() {
		// TODO Auto-generated method stub
		sendMsg();

	}

	public void sendMsg() {
		String msg = msgPanel.getInputMsg();
		if (msg.length() == 0) {
			JOptionPane.showMessageDialog(GroupMsg.this, "没有输入任何内容！");
			return;
		}
		ArrayList<User> selUser = userTreePanel.getSelUser();
		String inMsg = msgPanel.getInputMsg();
		for (int i = 0; i < selUser.size(); i++) {
			if (selUser.get(i).equals(UserManager.me))
				continue;
			DataPack dp = new DataPack();
			dp.setDataType(DataPackage.IPMSG_SENDMSG
					| DataPackage.IPMSG_SENDCHECKOPT);
			dp.setMsg(msg);
			dp.setSendToHostAddres(selUser.get(i).getIp());
			SendMsgInfo m = new SendMsgInfo(dp, GroupMsg.this, true);
			MsgTransport.instance.addMsg(m);
		}
		msgPanel.appendSendMsg(inMsg);
		msgPanel.setInputMsg("");
	}

	private void shakeWindow() {
		if (!isFocused()) {
			requestFocusInWindow();
		}
		Point pos = getLocation();
		int x, y;
		boolean leftAndRight = true;
		boolean topAndBottom = true;
		for (int i = 50; i >= 0; i -= 5) {
			leftAndRight = !leftAndRight;
			topAndBottom = !topAndBottom;
			x = pos.x + (leftAndRight ? -i : i);
			// if (!leftAndRight) {
			// y = pos.y + (topAndBottom ? -i : i);
			// } else
			y = pos.y;
			setLocation(x, y);
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		setLocation(pos);
	}

	@Override
	public void onSendWindowShake() {
		// TODO Auto-generated method stub
		shakeWindow();
		ArrayList<User> selUser = UserManager.instance.getAllUser();
		for (int i = 0; i < selUser.size(); i++) {
			DataPack dp = new DataPack();
			dp.setDataType(DataPackage.IPMSG_SENDSHAKE
					| DataPackage.IPMSG_SENDCHECKOPT);
			dp.setSendToHostAddres(selUser.get(i).getIp());
			SendMsgInfo smi = new SendMsgInfo(dp, this, false);
			MsgTransport.instance.addMsg(smi);
			msgPanel.appendSystemMsg("您向" + selUser.get(i).getName()
					+ "发送了一个窗口抖动。");
		}
	}

	@Override
	public void onStartInput() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEndInput() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLoadFinish() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSendJMsgApplicationEvent(String msg) {
		// TODO Auto-generated method stub

	}
}
