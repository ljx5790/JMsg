package org.zhangkun.jmsg.ui.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import org.zhangkun.jmsg.bean.User;
import org.zhangkun.jmsg.core.UserManager;
import org.zhangkun.jmsg.core.listener.UserListener;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FindUserDialog extends JDialog implements UserListener {

	private final JPanel contentPanel = new JPanel();
	private JTextField keyField;
	private JCheckBox checkName;
	private JCheckBox checkIp;
	private JCheckBox checkGroup;
	private JComboBox applicationVersion;
	private JLabel label_1;
	private JTable resultTable;
	private JScrollPane scrollPane;
	private JPanel panel_1;
	private JLabel label_2;
	private JLabel resultNum;
	private JLabel label_3;
	private JCheckBox checkHostname;
	private ArrayList<User> allResultUsers = new ArrayList<User>();
	private JLabel label_4;

	/**
	 * Create the dialog.
	 */
	public FindUserDialog() {
		UserManager.addUserListener(this);
		setTitle("查找");
		setModal(true);
		setBounds(100, 100, 460, 300);
		BorderLayout borderLayout = new BorderLayout();
		borderLayout.setVgap(5);
		borderLayout.setHgap(5);
		getContentPane().setLayout(borderLayout);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JPanel panel = new JPanel();
			FlowLayout flowLayout = (FlowLayout) panel.getLayout();
			flowLayout.setAlignment(FlowLayout.LEFT);
			contentPanel.add(panel, BorderLayout.NORTH);
			{
				label_1 = new JLabel("版本匹配：");
				panel.add(label_1);
			}
			{
				applicationVersion = new JComboBox();
				applicationVersion.addItemListener(new ItemListener() {
					public void itemStateChanged(ItemEvent e) {
						find();
					}
				});
				applicationVersion.setModel(new DefaultComboBoxModel(
						new String[] { "全部", "JMsg", "飞鸽", "飞秋" }));
				panel.add(applicationVersion);
			}
			{
				JLabel label = new JLabel("查找项：");
				panel.add(label);
			}
			{
				checkName = new JCheckBox("姓名");
				checkName.setSelected(true);
				checkName.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						find();
					}
				});
				panel.add(checkName);
			}
			{
				checkIp = new JCheckBox("IP");
				checkIp.setSelected(true);
				checkIp.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						find();
					}
				});
				panel.add(checkIp);
			}
			{
				checkGroup = new JCheckBox("组名 ");
				checkGroup.setSelected(true);
				checkGroup.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						find();
					}
				});
				panel.add(checkGroup);
			}
			{
				checkHostname = new JCheckBox("主机名称");
				checkHostname.setSelected(true);
				checkHostname.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						find();
					}
				});
				panel.add(checkHostname);
			}
		}
		{
			resultTable = new JTable();
			resultTable.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() > 1) {
						dispose();
						MainWindow.instance.getUserTreePanel()
								.scrollTo(
										allResultUsers.get(resultTable
												.getSelectedRow()));
					}
				}
			});
			resultTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		{
			scrollPane = new JScrollPane(resultTable);
			contentPanel.add(scrollPane, BorderLayout.CENTER);
		}
		{
			panel_1 = new JPanel();
			contentPanel.add(panel_1, BorderLayout.SOUTH);
			panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
			{
				label_2 = new JLabel("搜索到 ");
				panel_1.add(label_2);
			}
			{
				resultNum = new JLabel("0");
				panel_1.add(resultNum);
			}
			{
				label_3 = new JLabel(" 个用户");
				panel_1.add(label_3);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new EmptyBorder(5, 5, 0, 5));
			getContentPane().add(panel, BorderLayout.NORTH);
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			{
				keyField = new JTextField();
				keyField.getDocument().addDocumentListener(
						new DocumentListener() {

							@Override
							public void removeUpdate(DocumentEvent e) {
								// TODO Auto-generated method stub
								find();
							}

							@Override
							public void insertUpdate(DocumentEvent e) {
								// TODO Auto-generated method stub
								find();
							}

							@Override
							public void changedUpdate(DocumentEvent e) {
								// TODO Auto-generated method stub
							}
						});
				{
					label_4 = new JLabel("搜索关键字：");
					panel.add(label_4);
				}
				panel.add(keyField);
				keyField.setColumns(10);
			}
		}
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				find();
			}

			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				dispose();
				UserManager.removeUserListener(FindUserDialog.this);
			}
		});
		setVisible(true);
	}

	private String getVersionStr(int version) {
		if (version == User.CLIENT_FQMSG)
			return "飞秋";
		if (version == User.CLIENT_IPMSG)
			return "飞鸽";
		if (version == User.CLIENT_JMSG)
			return "JMsg";
		return "";
	}

	private void find() {
		String key = "";
		if (keyField != null)
			key = keyField.getText();
		DefaultTableModel dtm = new DefaultTableModel() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -76698199148477097L;

			@Override
			public boolean isCellEditable(int row, int column) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		allResultUsers.clear();
		dtm.addColumn("版本");
		dtm.addColumn("组名");
		dtm.addColumn("呢称");
		dtm.addColumn("IP");
		dtm.addColumn("主机名称");
		ArrayList<User> allUsers = UserManager.instance.getAllUser();
		for (int i = 0; i < allUsers.size(); i++) {
			Vector<String> data = new Vector<String>();
			User user = allUsers.get(i);

			int version = user.getVersion();
			int index = applicationVersion.getSelectedIndex();
			if (index != 0) {
				if (index == 1 && version != User.CLIENT_JMSG)
					continue;
				if (index == 2 && version != User.CLIENT_IPMSG)
					continue;
				if (index == 3 && version != User.CLIENT_FQMSG)
					continue;
			}

			if (key.equals("")) {
				data.add(getVersionStr(user.getVersion()));
				data.add(user.getGroup());
				data.add(user.getName());
				data.add(user.getIp());
				data.add(user.getHostName());
				dtm.addRow(data);
				allResultUsers.add(user);
				continue;
			}
			if (checkName.isSelected()) {
				if (user.getName().indexOf(key) != -1) {
					data.add(getVersionStr(user.getVersion()));
					data.add(user.getGroup());
					data.add(user.getName());
					data.add(user.getIp());
					data.add(user.getHostName());
					dtm.addRow(data);
					allResultUsers.add(user);
					continue;
				}
			}
			if (checkIp.isSelected()) {
				if (user.getIp().indexOf(key) != -1) {
					data.add(getVersionStr(user.getVersion()));
					data.add(user.getGroup());
					data.add(user.getName());
					data.add(user.getIp());
					data.add(user.getHostName());
					dtm.addRow(data);
					allResultUsers.add(user);
					continue;
				}
			}
			if (checkGroup.isSelected()) {
				if (user.getGroup().indexOf(key) != -1) {
					data.add(getVersionStr(user.getVersion()));
					data.add(user.getGroup());
					data.add(user.getName());
					data.add(user.getIp());
					data.add(user.getHostName());
					dtm.addRow(data);
					allResultUsers.add(user);
					continue;
				}
			}
			if (checkHostname.isSelected()) {
				if (user.getHostName().indexOf(key) != -1) {
					data.add(getVersionStr(user.getVersion()));
					data.add(user.getGroup());
					data.add(user.getName());
					data.add(user.getIp());
					data.add(user.getHostName());
					dtm.addRow(data);
					allResultUsers.add(user);
					continue;
				}
			}

		}
		resultTable.setModel(dtm);
		resultNum.setText(dtm.getRowCount() + "");
	}

	@Override
	public void addUser(User u) {
		// TODO Auto-generated method stub
		find();
	}

	@Override
	public void delAllUser() {
		// TODO Auto-generated method stub
		find();
	}

	@Override
	public void modifyUser(User u) {
		// TODO Auto-generated method stub
		find();
	}

	@Override
	public void removeUser(User u) {
		// TODO Auto-generated method stub
		find();
	}

	public JLabel getResultNum() {
		return resultNum;
	}
}
