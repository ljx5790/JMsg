package org.zhangkun.jmsg.ui.swing.listener;

public interface MsgViewListener {
	public abstract void onSendMsgEvent();

	public abstract void onSendWindowShake();

	public abstract void onStartInput();

	public abstract void onEndInput();

	public abstract void onSendJMsgApplicationEvent(String msg);

	public abstract void onLoadFinish();

}
