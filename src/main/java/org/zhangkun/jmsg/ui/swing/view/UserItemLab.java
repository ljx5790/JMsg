package org.zhangkun.jmsg.ui.swing.view;

import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JPanel;

import org.zhangkun.jmsg.bean.User;

public class UserItemLab extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5151066468452085136L;
	private User user;

	public UserItemLab() {
	}

	public User getUser() {
		return user;
	}

	// @Override
	public void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		// g.setColor(SystemColor.activeCaption);
		FontMetrics fm = g.getFontMetrics();
		int h = fm.getHeight() * 2;
		g.drawImage(user.getPhoto(), 3, 3, h, h, this);
		g.drawString(user.getName(), h + 6, 3 + fm.getAscent());
		g.drawString(user.getIp(), h + 6, 3 + h / 2 + fm.getAscent());
	}

	public void setUser(User user) {
		this.user = user;
		int nw = getFontMetrics(getFont()).stringWidth(user.getName()) + 40;
		int iw = getFontMetrics(getFont()).stringWidth(user.getIp()) + 40;
		setPreferredSize(new Dimension(nw > iw ? nw : iw, (getFontMetrics(
				getFont()).getHeight() + 3) * 2));
	}

}
