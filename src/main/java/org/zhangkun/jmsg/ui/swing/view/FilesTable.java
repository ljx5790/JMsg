package org.zhangkun.jmsg.ui.swing.view;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.zhangkun.jmsg.core.network.filetransport.FileMsgPackage;
import org.zhangkun.jmsg.util.FileUtil;
import org.zhangkun.jmsg.util.Res;

import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.Serializable;

public class FilesTable extends JPanel {
	public class thisItemRenderer extends DefaultTableCellRenderer {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3114377991820790546L;

		@Override
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			// TODO Auto-generated method stub
			setHorizontalAlignment(SwingConstants.CENTER);
			// if (row % 2 == 0)
			// color = new Color(206, 231, 255);
			// else
			// color = new Color(255, 255, 255);
			if (isSelected) {
				setBackground(table.getSelectionBackground());
				setForeground(table.getSelectionForeground());
			} else {
				setBackground(table.getBackground());
				setForeground(table.getForeground());
			}
			setIcon(null);
			if (value instanceof JCheckBox) {
				if (isSelected) {
					((JCheckBox) value).setBackground(table
							.getSelectionBackground());
					((JCheckBox) value).setForeground(table
							.getSelectionForeground());
				} else {
					((JCheckBox) value).setBackground(table.getBackground());
					((JCheckBox) value).setForeground(table.getForeground());
				}
				((JCheckBox) value)
						.setHorizontalAlignment(SwingConstants.CENTER);
				return (JCheckBox) value;
			} else if (value instanceof ImageIcon) {
				setIcon((ImageIcon) value);
				value = "";

			} else {
				if (column == 3)
					value = (String) FileUtil.getSizeStr((Long) value);
			}
			return super.getTableCellRendererComponent(table, value,
					isSelected, hasFocus, row, column);
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3575963853398545009L;

	private JLabel acceptNumLabel;
	private DefaultTableModel dtm = new DefaultTableModel() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public boolean isCellEditable(int row, int column) {
			// TODO Auto-generated method stub
			return false;
		}

	};
	private ArrayList<FileMsgPackage> fmps;
	private Component okButton;

	private JTable table = new JTable();

	public FilesTable() {
		setLayout(new BorderLayout());
		JScrollPane scrollPane = new JScrollPane(table);
		this.add(scrollPane);

		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		final JCheckBox checkBox = new JCheckBox("全选");
		checkBox.setSelected(true);
		checkBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < table.getRowCount(); i++) {
					JCheckBox cb = (JCheckBox) dtm.getValueAt(i, 0);
					cb.setSelected(checkBox.isSelected());
					fmps.get(i).setEnable(cb.isSelected());
					updateAcceptNum();
					updateUI();
				}
			}
		});
		panel.add(checkBox);

		acceptNumLabel = new JLabel("");
		panel.add(acceptNumLabel);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = table.getSelectedRow();
				int col = table.getSelectedColumn();
				if (col == 0) {
					JCheckBox cb = (JCheckBox) dtm.getValueAt(row, col);
					cb.setSelected(!cb.isSelected());
					fmps.get(row).setEnable(cb.isSelected());
					updateAcceptNum();
					updateUI();
				}
			}
		});
	}

	public void addPath(FileMsgPackage fmp) {
		Vector<Serializable> v = new Vector<Serializable>();
		JCheckBox cb = new JCheckBox();
		cb.setSelected(true);
		v.addElement(cb);
		// JLabel label = new JLabel();
		if (fmp.getFileMsgType() == 1)
			v.addElement(new ImageIcon(Res.FILE_IMG));
		else
			v.addElement(new ImageIcon(Res.FOLDER_IMG));
		// v.addElement(label);
		v.addElement(fmp.getFileName());
		v.addElement(fmp.getFileSize());

		dtm.addRow(v);
		table.setModel(dtm);
		table.setDefaultRenderer(Object.class, new thisItemRenderer());
		table.getColumnModel().getColumn(0).setPreferredWidth(5);
		table.getColumnModel().getColumn(1).setPreferredWidth(5);
		table.doLayout();
		for (int i = 0; i < dtm.getRowCount(); i++)
			table.setRowHeight(i, 40);
	}

	public DefaultTableModel getDtm() {
		return dtm;
	}

	public Component getOkButton() {
		return okButton;
	}

	public void init(ArrayList<FileMsgPackage> fmps, String name) {
		// TODO Auto-generated method stub
		this.fmps = fmps;
		dtm.addColumn(name);
		dtm.addColumn("类型");
		dtm.addColumn("名称");
		dtm.addColumn("大小");
		for (int i = 0; i < fmps.size(); i++) {
			FileMsgPackage fmp = fmps.get(i);
			addPath(fmp);
		}
	}

	public void setDtm(DefaultTableModel dtm) {
		this.dtm = dtm;
	}

	public void setOkButton(Component okButton) {
		this.okButton = okButton;
	}

	public void updateAcceptNum() {
		int j = 0;
		for (int i = 0; i < table.getRowCount(); i++) {
			if (((JCheckBox) table.getModel().getValueAt(i, 0)).isSelected())
				j++;
		}
		if (okButton != null)
			if (j == 0) {
				okButton.setEnabled(false);
				acceptNumLabel
						.setText("<html><body><font color=red>您没有选中任何文件！</font></body></html>");
			} else {
				acceptNumLabel.setText("已选中：" + j + " 项");
				okButton.setEnabled(true);
			}

	}

}
