package org.zhangkun.jmsg.util;

import java.util.List;

public class StrUtil {

	public static String formatTime(long time) {
		if (time < 1000)
			return time + "毫秒";
		time /= 1000;
		if (time < 60)
			return time + "秒";
		if (time < 60 * 60)
			return time / 60 + "分" + time % 60 + "秒";
		if (time < 60 * 60 * 60)
			return time / (60 * 60) + "小时" + time % (60 * 60) + "分" + time % 60
					+ "秒";
		return null;
	}

	public static String joinStrings(Object[] objects, String tag) {
		String str = "";
		for (int i = 0; i < objects.length; i++)
			str += objects[i].toString() + tag;
		return str.substring(0, str.length() - tag.length());
	}

	public static String joinStrings(List objects, String tag) {
		String str = "";
		for (int i = 0; i < objects.size(); i++)
			str += objects.get(i).toString() + tag;
		return str.substring(0, str.length() - tag.length());
	}

	public static boolean verifyIPAddres(String ip) {
		return ip.matches("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");
	}
}
