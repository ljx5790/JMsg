package org.zhangkun.jmsg.util;

import java.io.Serializable;
import java.util.ArrayList;

import org.zhangkun.jmsg.bean.User;

public class GlobalVar implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2258154776705603627L;

	/**
	 * 直接弹出文件
	 */
	public static boolean FILE_DIRECT_POP_UP = false;

	/**
	 * 最大传输队列
	 */
	public static int MAX_TRANSPORT_QUEUE = 100;

	/**
	 * 直接弹出消息
	 */
	public static boolean MSG_DIRECT_POP_UP = false;

	/**
	 * 传输编码
	 */
	public static String TRANSPORT_ENCODING = "gb2312";

	/**
	 * 文件托盘提示
	 */
	public static boolean TRAY_FILE_MSG = true;

	/**
	 * 消息托盘提示
	 */
	public static boolean TRAY_MSG = true;

	/**
	 * 通信端口
	 */
	public final static int IPMSG_PORT = 2425;

	/**
	 * 服务器端口
	 */
	public final static int HTTP_PORT = 2426;

	/**
	 * 扫描线程
	 */
	public static int SCAN_THREADS = 20;

	/**
	 * 用户名
	 */
	public static String NAME = "";
	/**
	 * 用户的组名
	 */
	public static String GROUP = User.NOGROUPSTR;

	public static ArrayList<String> SCAN_IP = new ArrayList<String>();

}
