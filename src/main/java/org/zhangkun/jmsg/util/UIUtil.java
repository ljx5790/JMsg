package org.zhangkun.jmsg.util;


import javax.swing.*;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.Enumeration;

/**
 * 
 * @author zhangkun
 * 
 */
public class UIUtil {

//	private static com.nilo.plaf.nimrod.NimRODLookAndFeel nr = new NimRODLookAndFeel();

	public static void enable(Component[] cs, boolean enable) {
		if (cs == null)
			return;
		for (int i = 0; i < cs.length; i++) {
			cs[i].setEnabled(enable);
			if (cs[i] instanceof JComponent)
				enable((JComponent) cs[i], enable);
		}
	}

	public static void enable(JComponent parent, boolean enable) {
		parent.setEnabled(enable);
		enable(parent.getComponents(), enable);
	}

	public static void FitTableColumns(JTable myTable) {
		JTableHeader header = myTable.getTableHeader();
		int rowCount = myTable.getRowCount();
		Enumeration columns = myTable.getColumnModel().getColumns();
		while (columns.hasMoreElements()) {
			TableColumn column = (TableColumn) columns.nextElement();
			int col = header.getColumnModel().getColumnIndex(
					column.getIdentifier());
			// 取得表头宽度,然后和内容宽度进行逐一比较,取最大者,如果不想表格宽度随表头变化的话就将下面的width设为0
			int width = (int) myTable
					.getTableHeader()
					.getDefaultRenderer()
					.getTableCellRendererComponent(myTable,
							column.getIdentifier(), false, false, -1, col)
					.getPreferredSize().getWidth();
			for (int row = 0; row < rowCount; row++) {
				int preferedWidth = (int) myTable
						.getCellRenderer(row, col)
						.getTableCellRendererComponent(myTable,
								myTable.getValueAt(row, col), false, false,
								row, col).getPreferredSize().getWidth();
				width = Math.max(width, preferedWidth);
			}
			header.setResizingColumn(column); // 此行很重要
			column.setWidth(width + 20 + myTable.getIntercellSpacing().width);
		}
	}

	public static void initTheme() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		// URL url = UIUtil.class.getResource("/res/Snow.theme");
		// NimRODTheme nt = new NimRODTheme(url);
		// NimRODLookAndFeel.setCurrentTheme(nt);
		// updateLookAndFeel(nr);

		// 装载可选择的主题
		try {
			// 设置外观
//			UIManager
//					.setLookAndFeel(new SubstanceBusinessBlackSteelLookAndFeel());
//			// 设置主题
//			SubstanceLookAndFeel.setCurrentTheme(new SubstanceLightAquaTheme());
//			// // 设置按钮外观
//			SubstanceLookAndFeel
//					.setCurrentButtonShaper(new StandardButtonShaper());
//			// // 设置水印
//			SubstanceLookAndFeel
//					.setCurrentWatermark(new SubstanceBubblesWatermark());
//			// // 设置边框
//			SubstanceLookAndFeel
//					.setCurrentBorderPainter(new SimplisticSoftBorderPainter());
//			// // 设置渐变渲染
//			SubstanceLookAndFeel
//					.setCurrentGradientPainter(new StandardGradientPainter());
//			// // 设置标题
//			SubstanceLookAndFeel.setCurrentTitlePainter(new ArcHeaderPainter());

			// SubstanceLookAndFeel.setSkin(new OfficeBlue2007Skin());

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void updateLookAndFeel(JFrame frame) {
		SwingUtilities.updateComponentTreeUI(frame);
	}

	public static void updateLookAndFeel(LookAndFeel lnfName) {
		try {
			UIManager.setLookAndFeel(lnfName);
		} catch (UnsupportedLookAndFeelException ex1) {
			System.err.println("Unsupported LookAndFeel: " + lnfName);
		}
	}

	public static void updateLookAndFeel(String lnfName) {
		try {
			UIManager.setLookAndFeel(lnfName);
		} catch (UnsupportedLookAndFeelException ex1) {
			System.err.println("Unsupported LookAndFeel: " + lnfName);
		} catch (ClassNotFoundException ex2) {
			System.err.println("LookAndFeel class not found: " + lnfName);
		} catch (InstantiationException ex3) {
			System.err.println("Could not load LookAndFeel: " + lnfName);
		} catch (IllegalAccessException ex4) {
			System.err.println("Cannot use LookAndFeel: " + lnfName);
		}
	}
}
