package org.zhangkun.jmsg.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.zhangkun.jmsg.bean.User;

public class ConfigManager {

	public final static String CONFIG_FILENAME = ".jmsg";

	public static void loadConfig(Properties p) {
		GlobalVar.NAME = p.getProperty("NAME",
				System.getProperty("user.name", "NoName"));
		GlobalVar.GROUP = p.getProperty("GROUP", User.NOGROUPSTR);
		GlobalVar.FILE_DIRECT_POP_UP = changeStrToBol(
				p.getProperty("FILE_DIRECT_POP_UP"), false);
		GlobalVar.MSG_DIRECT_POP_UP = changeStrToBol(
				p.getProperty("MSG_DIRECT_POP_UP"), false);
		GlobalVar.SCAN_THREADS = changeStrToInt(p.getProperty("SCAN_THREADS"),
				20);
		GlobalVar.TRANSPORT_ENCODING = p.getProperty("TRANSPORT_ENCODING",
				"gb2312");
		GlobalVar.TRAY_MSG = changeStrToBol(p.getProperty("TRAY_MSG"), true);
		GlobalVar.TRAY_FILE_MSG = changeStrToBol(
				p.getProperty("TRAY_FILE_MSG"), true);
		String ipsStr = p.getProperty("SCAN_IP");
		if (ipsStr != null) {
			String[] ips = ipsStr.split("\\|");
			GlobalVar.SCAN_IP.clear();
			for (int i = 0; i < ips.length; i++)
				GlobalVar.SCAN_IP.add(ips[i]);
		}
	}

	public static void saveConfig(Properties p) {
		p.setProperty("NAME", GlobalVar.NAME);
		p.setProperty("GROUP", GlobalVar.GROUP);
		p.setProperty("FILE_DIRECT_POP_UP", GlobalVar.FILE_DIRECT_POP_UP + "");
		p.setProperty("MSG_DIRECT_POP_UP", GlobalVar.MSG_DIRECT_POP_UP + "");
		p.setProperty("SCAN_THREADS", GlobalVar.SCAN_THREADS + "");
		p.setProperty("TRANSPORT_ENCODING", GlobalVar.TRANSPORT_ENCODING);
		p.setProperty("TRAY_MSG", GlobalVar.TRAY_MSG + "");
		p.setProperty("TRAY_FILE_MSG", GlobalVar.TRAY_FILE_MSG + "");
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < GlobalVar.SCAN_IP.size(); i++)
			sb.append(GlobalVar.SCAN_IP.get(i) + "|");
		p.setProperty("SCAN_IP", sb.toString());
	}

	public static boolean changeStrToBol(String str, boolean defaultBol) {
		if (str != null) {
			if (str.equalsIgnoreCase("true"))
				return true;
			if (str.equalsIgnoreCase("false"))
				return false;
		}
		return defaultBol;
	}

	public static int changeStrToInt(String str, int defaultInt) {
		if (str != null) {
			try {
				return Integer.parseInt(str);
			} catch (Exception e) {
			}
		}
		return defaultInt;
	}

	public static void loadConfig() {
		File f = new File(System.getProperty("user.dir") + File.separator
				+ CONFIG_FILENAME);
		System.out.println("loadConfig from " + f.getAbsolutePath());
		Properties p = new Properties();
		if (f.exists()) {
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(f);
				p.load(fis);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (fis != null)
					try {
						fis.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		loadConfig(p);
	}

	public static void saveConfig() {
		File f = new File(System.getProperty("user.dir") + File.separator
				+ CONFIG_FILENAME);
		Properties p = new Properties();
		saveConfig(p);
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(f);
			p.store(fos, "JMsg Application Config File!");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (fos != null)
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

}
