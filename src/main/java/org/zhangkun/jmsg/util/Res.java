package org.zhangkun.jmsg.util;

import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Res {
	public static Image CANCEL_IMG;
	public static Image DOWN_IMG;
	public static Image FILE_IMG;

	public static Image FOLDER_IMG;
	public static Image FQ_IMG;
	public static Image GROUP_CLOSE;
	public static Image GROUP_OPEN;
	public static Image IPMSG_IMG;
	public static Image JMSG_LOGO_IMG;
	public static Image MSG_IMG;
	public static Image UP_IMG;
	static {
		try {
			DOWN_IMG = ImageIO.read((Res.class.getResource("/res/down.png")));
			UP_IMG = ImageIO.read(Res.class.getResource("/res/up.png"));
			JMSG_LOGO_IMG = ImageIO.read(Res.class.getResource("/res/M8Msg.png"));
			CANCEL_IMG = ImageIO.read(Res.class.getResource("/res/cancel.png"));
			FILE_IMG = ImageIO.read(Res.class.getResource("/res/file.png"));
			FOLDER_IMG = ImageIO.read(Res.class.getResource("/res/folder.png"));
			FQ_IMG = ImageIO.read(Res.class.getResource("/res/fq.png"));
			IPMSG_IMG = ImageIO.read(Res.class.getResource("/res/ipmsg.png"));
			MSG_IMG = ImageIO.read(Res.class.getResource("/res/msg.png"));
			GROUP_OPEN = ImageIO.read(Res.class
					.getResource("/res/groupopen.png"));
			GROUP_CLOSE = ImageIO.read(Res.class
					.getResource("/res/groupclose.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// System.out.println("资源文件加载错误!");
			// JOptionPane.showMessageDialog(null, "资源文件加载错误!");
			// System.exit(0);
		}
		// System.out.println("资源文件加载完毕!");
	}
}
