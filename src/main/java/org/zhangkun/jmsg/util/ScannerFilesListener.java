package org.zhangkun.jmsg.util;

public interface ScannerFilesListener {
	public abstract void finishScan(String path, long allSize, boolean b);

	public abstract void progress(String path, int index, int allLength);
}
