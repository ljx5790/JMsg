package org.zhangkun.jmsg.webserver.server;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;


/**
 * 
 * 
 * Copyright (C), 2006-2010, ChengDu Lovo info. Co., Ltd. FileName: Request.java
 * �����װ�࣬�����������
 * 
 * @author zhangkun
 * @Date 2011-6-17
 * @version 1.00
 */
public class Request {

	public final static int REQUEST_TYPE_GET = 1;
	public final static int REQUEST_TYPE_POST = 2;

	private byte[] content;

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	// ��ݿ�
	private String data;

	// ����ͷ
	private HashMap<String, String> header;

	// �����б�
	private HashMap<String, String> parameters;

	private HashMap<String, String> allCookie = new HashMap<String, String>();

	/**
	 * ��������ʽ,Ĭ��utf-8
	 */
	private String parseEncoding = "utf-8";

	// ������ļ�·��
	private String path;

	/**
	 * ��������
	 */
	private int requestType;

	// ���������URL
	private String url;

	public Request(ByteArrayOutputStream byteArrayOutputStream) {
		this.content = byteArrayOutputStream.toByteArray();
		header = new HashMap<String, String>();
		parameters = new HashMap<String, String>();
		parseURL();
		parseHeader();
		parseCookie();
	}

	/**
	 * ����Cookie
	 */
	private void parseCookie() {
		// TODO Auto-generated method stub
		String cookies = header.get("Cookie");
		if (cookies != null) {
			String[] cs = cookies.split(";");
			for (int i = 0; i < cs.length; i++) {
				String[] tmp = cs[i].split("=", 2);
				if (tmp != null && tmp.length == 2)
					allCookie.put(tmp[0].trim(), tmp[1].trim());
			}
		}
	}

	public String getCookie(String name) {
		return allCookie.get(name);
	}

	public HashMap<String, String> getAllCookie() {
		return allCookie;
	}

	public void setAllCookie(HashMap<String, String> allCookie) {
		this.allCookie = allCookie;
	}

	/**
	 * ��ȡ�������ݿ�
	 * 
	 * @return
	 */
	public String getData() {
		return data;
	}

	/**
	 * ��ȡ����ͷ��HashMap
	 * 
	 * @return
	 */
	public HashMap<String, String> getHeader() {
		return header;
	}

	public String getHeader(String string) {
		// TODO Auto-generated method stub
		return header.get(string);
	}

	/**
	 * ȡ�ò���
	 * 
	 * @param name
	 *            �������
	 * @return ����ֵ
	 */
	public String getParameter(String name) {
		return parameters.get(name);
	}

	/**
	 * ��ȡ���в���
	 * 
	 * @return
	 */
	public HashMap<String, String> getParameters() {
		return parameters;
	}

	/**
	 * @return the parseEncoding
	 */
	public String getParseEncoding() {
		return parseEncoding;
	}

	/**
	 * ��ȡ������ļ�·��
	 * 
	 * @return
	 */
	public String getPath() {
		return path;
	}

	/**
	 * ȡ����������
	 * 
	 * @return
	 */
	public int getRequestType() {
		return requestType;
	}

	/**
	 * ��ȡ���������URL
	 * 
	 * @return
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * ��������
	 * 
	 * @return
	 */
	public void parseHeader() {
		String[] content = null;
		try {
			content = new String(this.content, parseEncoding).split("\r\n\r\n",
					2);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			content = new String(this.content).split("\r\n\r\n", 2);
		}
		String[] hd = content[0].split("\n");
		for (int i = 0; i < hd.length; i++) {
			String[] tmp = hd[i].split(":", 2);
			if (tmp.length == 2)
				header.put(tmp[0].trim(), tmp[1].trim());
		}
		if (content.length == 2)
			data = content[1];
		if (requestType == REQUEST_TYPE_POST) {
			// System.out.println("post data=" + data);
			parseParameters(data);
		}
	}

	/**
	 * �����������
	 * 
	 * @param str
	 */
	private void parseParameters(String str) {
		if (str == null)
			return;
		String[] tmp = str.split("&");
		if (tmp == null)
			return;
		for (int i = 0; i < tmp.length; i++) {
			String[] t = tmp[i].split("=", 2);
			if (t != null && t.length == 2)
				parameters.put(t[0], t[1]);
		}
	}

	private void parseURL() {
		// TODO Auto-generated method stub
		int firstLine = 0;
		while (firstLine < content.length && content[firstLine] != '\n')
			firstLine++;
		firstLine++;
		String hd = new String(content, 0, firstLine);
		byte[] tmp = new byte[this.content.length - firstLine];
		System.arraycopy(content, firstLine, tmp, 0, tmp.length);
		this.content = tmp;
		requestType = hd.substring(0, 3).equals("GET") ? REQUEST_TYPE_GET
				: REQUEST_TYPE_POST;
		url = hd.split(" ")[1];
		int pi = url.indexOf("?");
		if (pi == -1)
			path = url;
		else {
			path = url.substring(0, pi);
			parseURLParameters(url.substring(pi + 1, url.length()));
		}
	}

	private void parseURLParameters(String str) {
		// TODO Auto-generated method stub

		String[] tmp = str.split("&");
		for (int i = 0; i < tmp.length; i++) {
			String[] t = tmp[i].split("=", 2);
			if (t != null && t.length == 2)
				try {
					parameters.put(t[0], URLDecoder.decode(t[1], "utf-8"));
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	/**
	 * @param parseEncoding
	 *            the parseEncoding to set
	 */
	public void setParseEncoding(String parseEncoding) {
		this.parseEncoding = parseEncoding;
	}

	public void setPath(String path) {
		// TODO Auto-generated method stub
		this.path = path;
	}

}
