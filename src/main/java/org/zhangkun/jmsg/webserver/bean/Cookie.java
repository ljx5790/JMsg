package org.zhangkun.jmsg.webserver.bean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * 
 * 
 * Copyright (C), 2006-2010, ChengDu Lovo info. Co., Ltd. FileName: Cookie.java
 * Cookie�࣬��װCookie
 * 
 * @author zhangkun
 * @Date 2011-6-22
 * @version 1.00
 */
public class Cookie {
	public static DateFormat df;
	public static SimpleDateFormat pdf = new SimpleDateFormat(
			"yy-MM-dd HH:mm:ss");

	public static DateFormat getDf() {
		return df;
	}

	public static void setDf(DateFormat df) {
		Cookie.df = df;
	}

	private String date;
	private String name;

	private String path;

	private String value;

	public Cookie() {
		if (df == null) {
			df = new SimpleDateFormat("EEE, d-MMM-yyyy HH:mm:ss z",
					Locale.ENGLISH);
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
		}
	}

	public Cookie(String name, String value) {
		this();
		this.name = name;
		this.value = value;
	}

	public Cookie(String name, String value, String date) {
		this();
		this.name = name;
		this.date = date;
		this.value = value;
	}

	public Cookie(String name, String value, String date, String path) {
		this(name, value);
		this.date = date;
		this.path = path;
	}

	public String getDate() {
		return date;
	}

	public String getName() {
		return name;
	}

	public String getPath() {
		return path;
	}

	public String getValue() {
		return value;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void modifyOffsetDate(int type, int offset) {
		Date d = null;
		if (date == null) {
			d = new Date();
		} else {
			try {
				d = pdf.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				return;
			}
		}
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(type, c.get(type) + offset);
		this.date = pdf.format(c.getTime());
	}

	public void setCurrentOffestYear(int year) {
		modifyOffsetDate(Calendar.YEAR, year);
	}

	public void setCurrentOffestMonth(int month) {
		modifyOffsetDate(Calendar.MONTH, month);
	}

	public void setCurrentOffestDay(int day) {
		modifyOffsetDate(Calendar.DAY_OF_MONTH, day);
	}

	public void setCurrentOffestHour(int hour) {
		modifyOffsetDate(Calendar.HOUR_OF_DAY, hour);
	}

	public void setCurrentOffestMinute(int minute) {
		modifyOffsetDate(Calendar.MINUTE, minute);
	}

	public void setCurrentOffestSecond(int second) {
		modifyOffsetDate(Calendar.SECOND, second);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * ����HTTPЭ���ʽ�е�Cookie��ʽ
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(name + " = " + value);
		if (date != null)
			try {
				sb.append(";expires=" + df.format(pdf.parse(date)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if (path != null)
			sb.append(";path=" + path);
		return sb.toString();
	}
}
