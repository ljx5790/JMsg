package org.zhangkun.jmsg.webserver.inter;

import org.zhangkun.jmsg.webserver.server.Request;
import org.zhangkun.jmsg.webserver.server.Response;

public interface Servlet {

	public abstract void doRequest(Request rt, Response rs) throws Exception;

}
