package org.zhangkun.jmsg.webserver.config;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * 
 * 
 * Copyright (C), 2006-2010, ChengDu Lovo info. Co., Ltd. FileName:
 * LISConfig.java ����������
 * 
 * @author zhangkun
 * @Date 2011-6-19
 * @version 1.00
 */
public class PageConfig {

	/**
	 * �������ӳ��ҳ
	 */
	private HashMap<Integer, String> errpage = new HashMap<Integer, String>();

	private String indexPage;

	private String log4jXMLPath;

	public PageConfig() {
	}

	public String getErrorPagePath(int errcode) {
		return errpage.get(errcode);
	}

	public String getIndexPage() {
		return indexPage;
	}

	public String getLog4jXMLPath() {
		// TODO Auto-generated method stub
		return log4jXMLPath;
	}

	public boolean loadConfig(InputStream xmlFile) {
		// TODO Auto-generated method stub
		SAXReader reader = new SAXReader();
		try {
			Document doc = reader.read(xmlFile);
			Element rootElement = doc.getRootElement();
			Iterator<Element> allErrorpage = rootElement
					.elementIterator("errpage");
			while (allErrorpage.hasNext()) {
				Element servlet = allErrorpage.next();
				errpage.put(
						Integer.parseInt(servlet.attribute("code").getValue()),
						servlet.getText());
			}
			log4jXMLPath = rootElement.element("log4jXMLPath").getText();
			setIndexPage(rootElement.element("indexpage").getText().trim());
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void setIndexPage(String indexPage) {
		this.indexPage = indexPage;
	}

}
