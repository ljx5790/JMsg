package org.zhangkun.jmsg.webserver.config;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * 
 * 
 * Copyright (C), 2006-2010, ChengDu Lovo info. Co., Ltd. FileName:
 * LISStaticRes.java ��������̬�ļ����ã���Ҫ�����þ�̬�ļ���MEMI
 * 
 * @author zhangkun
 * @Date 2011-6-17
 * @version 1.00
 */
public class FileMemiConfig {

	/**
	 * ��ǰ���е�Servlet
	 */
	private HashMap<String, String> allFileType;

	public FileMemiConfig() {
	}

	/**
	 * �������·��ȡ��һ��Servlet�����ؿձ�ʾû��
	 * 
	 * @param path
	 * @return
	 */
	public String getMIME(String path) {
		int i = path.lastIndexOf(".");
		if (i != -1)
			path = path.substring(i + 1, path.length());
		return allFileType.get(path.toLowerCase());
	}

	public boolean loadConfig(InputStream inputStream) {
		// TODO Auto-generated method stub
		allFileType = new HashMap<String, String>();
		SAXReader reader = new SAXReader();
		try {
			Document doc = reader.read(inputStream);
			Element rootElement = doc.getRootElement();
			Iterator<Element> allServlets = rootElement.elementIterator();
			while (allServlets.hasNext()) {
				Element file = allServlets.next();
				allFileType.put(file.element("type").getText(),
						file.element("mime").getText());
			}
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			return false;
		}
		return true;
	}
}
