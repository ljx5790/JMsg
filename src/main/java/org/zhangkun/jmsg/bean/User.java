package org.zhangkun.jmsg.bean;

import java.awt.Image;

import org.zhangkun.jmsg.util.Res;

public class User {

	public final static int CLIENT_JMSG = 0;
	public final static int CLIENT_IPMSG = 1;
	public final static int CLIENT_FQMSG = 2;

	/**
	 * 用户的组
	 */
	private String group = "";
	/**
	 * 用户的主机名
	 */
	private String hostName = "";
	/**
	 * 用户的IP
	 */
	private String ip = "";

	/**
	 * 上次收到的数据包
	 */
	private DataUnPack lastRequestDataPackage;

	/**
	 * 上次发送的数据包
	 */
	private DataPack lastSendDataPackage;

	/**
	 * 用户的呢称
	 */
	private String name = "";

	/**
	 * 用户的头像
	 */
	private Image photo;

	/**
	 * 用户的用户名
	 */
	private String username = "";

	/**
	 * 用户的客户的版本
	 */
	private int version = CLIENT_IPMSG;

	/**
	 * 判断一个对象是否等于该用户，如果IP地址相同则认为等于该用户
	 */
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (o instanceof User) {
			User u = (User) o;
			if (u.getIp().equals(ip))
				return true;
		} else if (o instanceof DataPackage) {
			DataPackage dp = (DataPackage) o;
			if (dp.getSenderHostAddres().equals(ip)
					&& dp.getSenderHostName().equals(hostName))
				return true;
		}
		return false;
	}

	public final static String NOGROUPSTR = "未分组好友";

	public String getGroup() {
		if (group.equals(""))
			return NOGROUPSTR;
		return group;
	}

	public String getHostName() {
		return hostName;
	}

	public String getIp() {
		return ip;
	}

	public DataUnPack getLastRequestDataPackage() {
		return lastRequestDataPackage;
	}

	public DataPack getLastSendDataPackage() {
		return lastSendDataPackage;
	}

	public String getName() {
		return name;
	}

	public Image getPhoto() {
		return photo;
	}

	public String getUsername() {
		return username;
	}

	public void setGroup(String group) {
		if (group != null)
			this.group = group;
	}

	public void setHostName(String hostName) {
		if (hostName != null)
			this.hostName = hostName;
	}

	public void setIp(String ip) {
		if (ip != null)
			this.ip = ip;
	}

	public void setLastRequestDataPackage(DataUnPack lastRequestDataPackage) {
		this.lastRequestDataPackage = lastRequestDataPackage;
	}

	public void setLastSendDataPackage(DataPack lastSendDataPackage) {
		this.lastSendDataPackage = lastSendDataPackage;
	}

	public void setName(String name) {
		if (name != null)
			this.name = name;
	}

	public void setPhoto(Image photo) {
		if (photo != null)
			this.photo = photo;
	}

	public void setUsername(String username) {
		if (username != null)
			this.username = username;
	}

	public void setVersion(int version) {
		this.version = version;
		updatePhoto();
	}

	private void updatePhoto() {
		// TODO Auto-generated method stub
		if (version == CLIENT_IPMSG) {
			photo = Res.IPMSG_IMG;
		} else if (version == CLIENT_JMSG) {
			photo = Res.JMSG_LOGO_IMG;
		} else if (version == CLIENT_FQMSG) {
			photo = Res.FQ_IMG;
		}
	}

	public int getVersion() {
		// TODO Auto-generated method stub
		return version;
	}

}
