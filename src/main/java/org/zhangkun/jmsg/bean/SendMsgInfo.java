package org.zhangkun.jmsg.bean;

import org.zhangkun.jmsg.core.listener.RecvMsgListener;

public class SendMsgInfo {
	private DataPack dp;
	private DataUnPack dup;
	private long lastSendTime;
	private RecvMsgListener listener;
	private boolean repeat = true;

	private int repeatNum = 5;
	private int sendNum = 1;
	private Object otherData;

	public SendMsgInfo(DataPack dp, RecvMsgListener listener, boolean repeat) {
		this.dp = dp;
		this.listener = listener;
		this.repeat = repeat;
	}

	public DataPack getDp() {
		return dp;
	}

	public DataUnPack getDup() {
		return dup;
	}

	public long getLastSendTime() {
		return lastSendTime;
	}

	public RecvMsgListener getListener() {
		return listener;
	}

	public int getRepeatNum() {
		return repeatNum;
	}

	public int getSendNum() {
		return sendNum;
	}

	public boolean isRepeat() {
		return repeat;
	}

	public void setDp(DataPack dp) {
		this.dp = dp;
	}

	public void setDup(DataUnPack dup) {
		this.dup = dup;
	}

	public void setLastSendTime(long lastSendTime) {
		this.lastSendTime = lastSendTime;
	}

	public void setListener(RecvMsgListener listener) {
		this.listener = listener;
	}

	public void setRepeat(boolean repeat) {
		this.repeat = repeat;
	}

	public void setRepeatNum(int repeatNum) {
		this.repeatNum = repeatNum;
	}

	public void setSendNum(int send) {
		this.sendNum = send;
	}

	public void setOtherData(Object otherData) {
		this.otherData = otherData;
	}

	public Object getOtherData() {
		return otherData;
	}
}
