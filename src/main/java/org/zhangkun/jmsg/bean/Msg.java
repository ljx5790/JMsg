package org.zhangkun.jmsg.bean;

public class Msg {
	public static final int MSG_SEND = 1;
	public static final int MSG_RECEIVE = 2;
	public static final int MSG_SYSTEM = 3;
	public static final int MSG_SHAKE = 4;
	public static final int MSG_START_INPUT = 5;
	public static final int MSG_END_INPUT = 6;
	private String msg;
	private int type;

	public Msg() {
	}

	public Msg(int type) {
		this.type = type;
	}

	public Msg(String msg, int type) {
		super();
		this.msg = msg;
		this.type = type;
	}

	public String getMsg() {
		return msg;
	}

	public int getType() {
		return type;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setType(int type) {
		this.type = type;
	}

}
