package org.zhangkun.jmsg.bean;

import java.io.Serializable;

/**
 * 数据包
 * 
 * @author zhangkun
 * 
 */

public class DataPackage implements Serializable {

	public static long ID = (long) (Math.random() * 10000);
	// other opt
	public static final long IPMSG_ABSENCEOPT = 0x00000100L;
	public static final long IPMSG_ANSENTRY = 0x00000003L;

	public static final long IPMSG_ANSLIST = 0x00000017L;

	public static final long IPMSG_ANSPUBKEY = 0x00000073L;
	public static final long IPMSG_AUTORETOPT = 0x00002000L;
	public static final long IPMSG_BLOWFISH_128 = 0x00020000L;
	public static final long IPMSG_BLOWFISH_256 = 0x00040000L;

	public static final long IPMSG_BR_ABSENCE = 0x00000004L;
	public static final long IPMSG_BR_ENTRY = 0x00000001L;
	public static final long IPMSG_BR_EXIT = 0x00000002L;
	public static final long IPMSG_BR_ISGETLIST = 0x00000018L;

	public static final long IPMSG_BROADCASTOPT = 0x00000400L;
	/* ========== Constant Value ========== */
	public static final long IPMSG_COMMASK = 0x000000ffL;

	public static final long IPMSG_DELMSG = 0x00000031L;
	public static final long IPMSG_DIALUPOPT = 0x00010000L;

	public static final long IPMSG_ENCRYPTOPT = 0x00400000L;
	public static final long IPMSG_ENCRYPTOPTOLD = 0x00800000L;

	public static final long IPMSG_FILE_CREATETIME = 0x00000016L;
	public static final long IPMSG_FILE_DIR = 0x00000002L;

	public static final long IPMSG_FILE_MTIME = 0x00000014L;
	public static final long IPMSG_FILE_REGULAR = 0x00000001L;
	public static final int IPMSG_FILE_RETPARENT = 0x00000003;
	// public static final long IPMSG_FILEATTACHOPT = 2097408L;
	public static final long IPMSG_FILEATTACHOPT = 0x00200000L;
	// public static final long IPMSG_FILEATTACHOPT = 0x00200000L;
	public static final long IPMSG_GETDIRFILES = 0x00000062L;

	public static final long IPMSG_GETFILEDATA = 0x00000060L;
	public static final long IPMSG_GETINFO = 0x00000040L;
	public static final long IPMSG_GETLIST = 0x00000016L;
	public static final long IPMSG_GETPUBKEY = 0x00000072L;
	public static final long IPMSG_LISTGET_TIMER = 0x0104;
	public static final long IPMSG_LISTGETRETRY_TIMER = 0x0105;
	public static final long IPMSG_MULTICASTOPT = 0x00000800L;
	public static final long IPMSG_NEWMUTIOPT = 0x00040000L;
	public static final long IPMSG_NOLOGOPT = 0x00020000L;
	public static final long IPMSG_NOOPERATION = 0x00000000L;

	public static final long IPMSG_NOPOPUPOPT = 0x00001000L;
	public static final long IPMSG_OKGETLIST = 0x00000015L;
	public static final long IPMSG_OPTMASK = 0xffffff00L;
	public static final long IPMSG_PASSWORDOPT = 0x00008000L;
	public static final long IPMSG_RC2_128 = 0x00004000L;
	public static final long IPMSG_RC2_256 = 0x00008000L;
	public static final long IPMSG_RC2_40 = 0x00001000L;
	public static final long IPMSG_READMSG = 0x00000030L;

	public static final long IPMSG_RECVMSG = 0x00000021L;

	public static final long IPMSG_RELEASEFILES = 0x00000061L;
	public static final long IPMSG_RETRYOPT = 0x00004000L;
	public static final long IPMSG_RSA_1024 = 0x00000002L;
	public static final long IPMSG_RSA_2048 = 0x00000004L;

	// encrypt opt
	public static final long IPMSG_RSA_512 = 0x00000001L;
	public static final long IPMSG_SECRETOPT = 0x00000200L;

	// send opt
	public static final long IPMSG_SENDCHECKOPT = 0x00000100L;
	public static final long IPMSG_SENDINFO = 0x00000041L;
	public static final long IPMSG_SENDMSG = 0x00000020L;
	public static final long IPMSG_SERVEROPT = 0x00000200L;

	// JMsg Add option
	public static final long IPMSG_SENDSHAKE = 0x0000081L;
	public static final long IPMSG_SEND_STARTINPUT = 0x0000084L;
	public static final long IPMSG_SEND_ENDINPUT = 0x0000088L;

	public static final int MAXBUF = 8192;
	/* ========== end ========== */

	/**
	 * 标示ID
	 */
	private String dataID = String.valueOf(ID++);
	/**
	 * 消息类型
	 */
	private long dataType;
	/**
	 * 组
	 */
	private String group = "";
	/**
	 * 消息内容
	 */
	private String msg = "";
	/**
	 * 发送者主机地址
	 */
	private String senderHostAddres = "";
	/**
	 * 发送者主机名
	 */
	private String senderHostName = "";

	/**
	 * 发送者姓名
	 */
	private String senderName = "";

	/**
	 * 发送者用户名
	 */
	private String senderUsername;

	/**
	 * 接收者主机地址
	 */
	private String sendToHostAddres;
	/**
	 * 接收者主机名
	 */
	private String sendToHostName;
	/**
	 * 接收者用户名
	 */
	private String sendToUsername;

	/**
	 * 协议版本号
	 */
	private String version = "";

	public String getDataID() {
		return dataID;
	}

	public long getDataType() {
		return dataType;
	}

	public String getGroup() {
		return group;
	}

	public String getMsg() {
		return msg;
	}

	public String getSenderHostAddres() {
		return senderHostAddres;
	}

	public String getSenderHostName() {
		return senderHostName;
	}

	public String getSenderName() {
		return senderName;
	}

	public String getSenderUsername() {
		return senderUsername;
	}

	public String getSendToHostAddres() {
		return sendToHostAddres;
	}

	public String getSendToHostName() {
		return sendToHostName;
	}

	public String getSendToUsername() {
		return sendToUsername;
	}

	public String getVersion() {
		return version;
	}

	public void setDataID(String dataID) {
		this.dataID = dataID;
	}

	public void setDataType(long dataType) {
		this.dataType = dataType;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setSenderHostAddres(String senderHostAddres) {
		this.senderHostAddres = senderHostAddres;
	}

	public void setSenderHostName(String senderHostName) {
		this.senderHostName = senderHostName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public void setSenderUsername(String senderUsername) {
		this.senderUsername = senderUsername;
	}

	public void setSendToHostAddres(String sendToHostAddres) {
		this.sendToHostAddres = sendToHostAddres;
	}

	public void setSendToHostName(String sendToHostName) {
		this.sendToHostName = sendToHostName;
	}

	public void setSendToUsername(String sendToUsername) {
		this.sendToUsername = sendToUsername;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
