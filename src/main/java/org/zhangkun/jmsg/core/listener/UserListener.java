package org.zhangkun.jmsg.core.listener;

import org.zhangkun.jmsg.bean.User;

/**
 * 
 * Copyright (C), 2006-2010, ChengDu Lovo info. Co., Ltd. FileName:
 * UserListener.java
 * 
 * 监听所有用户的状态
 * 
 * @author zhangkun
 * @Date 2011-7-13
 * @version 1.00
 */
public interface UserListener {

	public abstract void addUser(User u);

	public abstract void delAllUser();

	public abstract void modifyUser(User u);

	public abstract void removeUser(User u);

}
