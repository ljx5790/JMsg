package org.zhangkun.jmsg.core.listener;

import org.zhangkun.jmsg.bean.SendMsgInfo;

public interface RecvMsgListener {
	public abstract void acceptRecvMsg(SendMsgInfo sendMsgInfo, boolean result);
}