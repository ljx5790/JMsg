package org.zhangkun.jmsg.core.listener;

public interface CancelFileTransportListener {

	public abstract boolean cancelFileTransportListener(String packageID);

}
