package org.zhangkun.jmsg.core.listener;

import java.net.Socket;

import org.zhangkun.jmsg.core.network.filetransport.FileMsgPackage;

public interface AcceptFileRequestListener {
	public abstract boolean acceptFileRequest(Socket socket, FileMsgPackage fmp);
}
