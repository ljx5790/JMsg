package org.zhangkun.jmsg.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.zhangkun.jmsg.bean.DataUnPack;
import org.zhangkun.jmsg.bean.User;
import org.zhangkun.jmsg.core.listener.UserListener;

/**
 * 管理用户的类，包括添加删除用户
 * 
 * @author zhangkun
 * 
 */
public class UserManager {

	private final static ArrayList<UserListener> allListener = new ArrayList<UserListener>();
	public final static UserManager instance = new UserManager();
	public static User me = new User();

	public static void addUserListener(UserListener ul) {
		allListener.add(ul);
	}

	public static User getMe() {
		return me;
	}

	public static void removeUserListener(UserListener ul) {
		allListener.remove(ul);
	}

	/**
	 * 当前所有存在的用户
	 */
	private ArrayList<User> allUser = new ArrayList<User>();

	private UserManager() {
		// TODO Auto-generated constructor stub
	}

	public synchronized void addUser(User u) {
		allUser.add(u);
		for (int i = 0; i < allListener.size(); i++) {
			allListener.get(i).addUser(u);
		}
	}

	/**
	 * 根据接收到的数据包创建一个用户
	 * 
	 * @param dup
	 * @return
	 */
	public synchronized User createUser(DataUnPack dup) {
		User u = new User();
		u.setName(dup.getSenderName());
		u.setIp(dup.getSenderHostAddres());
		u.setGroup(dup.getGroup());
		u.setHostName(dup.getSenderHostName());
		u.setVersion(convertToVersion(dup.getVersion()));
		return u;
	}

	public static int convertToVersion(String str) {
		if (str.startsWith("1_jmsg"))
			return User.CLIENT_JMSG;
		else if (str.startsWith("1_lb"))
			return User.CLIENT_FQMSG;
		else
			return User.CLIENT_IPMSG;
	}

	public synchronized void delAllUser() {
		// TODO Auto-generated method stub
		allUser.clear();
		for (int i = 0; i < allListener.size(); i++) {
			allListener.get(i).delAllUser();
		}
		addUser(me);
	}

	/**
	 * 根据收到的数据包删除一个用户
	 * 
	 * @param dup
	 */
	public void delUser(DataUnPack dup) {
		synchronized (allUser) {
			User du = null;
			for (User u : allUser)
				if (u.equals(dup)) {
					du = u;
					break;
				}
			if (du != null) {
				allUser.remove(du);
				for (int i = 0; i < allListener.size(); i++) {
					allListener.get(i).removeUser(du);
				}
			}
		}
	}

	public ArrayList<User> getAllUser() {
		return allUser;
	}

	public synchronized String[] getGroups() {
		// TODO Auto-generated method stub
		ArrayList<String> groups = new ArrayList<String>();
		for (User u : allUser) {
			groups.add(u.getGroup());
		}
		Set<String> s = new HashSet<String>(groups);
		String[] ret = new String[s.size()];
		s.toArray(ret);
		return ret;
	}

	/**
	 * 根据接收到的数据包返回对应的用户
	 * 
	 * @param dup
	 * @return
	 */
	public synchronized User getUser(DataUnPack dup) {
		for (User u : allUser)
			if (u.equals(dup))
				return u;
		// ArrayList<SendMsgDialog> us = MainWindow.instance.getAllSendDialog();
		// for (int i = 0; i < us.size(); i++) {
		// User u = us.get(i).getUser();
		// if (u.equals(dup)) {
		// us.get(i).userLoading();
		// addUser(u);
		// return u;
		// }
		// }

		return null;
	}

	public synchronized User[] getUser(String group) {
		// TODO Auto-generated method stub
		ArrayList<User> gu = new ArrayList<User>();
		for (User u : allUser)
			if (group.equals(u.getGroup()))
				gu.add(u);
		User[] ru = new User[gu.size()];
		for (int i = 0; i < ru.length; i++)
			ru[i] = gu.get(i);
		return ru;
	}

}
