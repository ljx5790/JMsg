package org.zhangkun.jmsg.core.network.filetransport;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;

import org.zhangkun.jmsg.bean.User;

public class FileTransport extends Thread implements Serializable {
	public final static String ACCEPT = "接收";
	public final static int BUFFER_SIZE = 1024;
	public final static String SEND = "发送";
	/**
	 * 
	 */
	private static final long serialVersionUID = 7492978657534507437L;
	public final static int STATUS_BY_STOP = 6;
	public final static int STATUS_ERROR = 4;
	public final static int STATUS_FINISH = 3;
	public final static int STATUS_RUNNING = 0;
	public final static int STATUS_USER_CANCEL = 5;
	public final static int STATUS_WAITFORSTART = 1;
	/**
	 * 总共读取文件的大小
	 */
	protected long allReadSize = 0;
	/**
	 * 总共传送文件的大小
	 */
	protected long allSize;
	protected byte[] buffer = new byte[BUFFER_SIZE];
	protected boolean cancleNow = false;

	protected long currentFileLength = 0;

	protected String currentFileName;
	protected long currentFileReadLength;

	/**
	 * 当前传输项总共读取文件的大小
	 */
	protected long currentTransportReadSize = 0;

	/**
	 * 当前传输项总共大小
	 */
	protected long currentTransportSize = 0;

	protected String error;

	/**
	 * 总共接收文件数
	 */
	protected long fileNum;

	protected ArrayList<FileMsgPackage> fmps = new ArrayList<FileMsgPackage>();

	/**
	 * 
	 */
	protected int fmpSize;

	protected int index;

	protected InputStream is;

	private long lastAllReadSize;

	private long lastCurrentTime;

	protected OutputStream os;

	/**
	 * 连接的套接字
	 */
	protected Socket socket;

	/**
	 * 传送速度
	 */
	protected int speed;

	private long[] speedBuffer = new long[3];

	protected int status = STATUS_WAITFORSTART;
	protected User user;
	protected String workName;

	public FileTransport(User user, String workName) {
		super();
		setName("FileTransport Thread");
		this.workName = workName;
		this.setUser(user);
	}

	public long getAllReadSize() {
		// TODO Auto-generated method stub
		return allReadSize;
	}

	public long getAllSize() {
		return allSize;
	}

	public byte[] getBuffer() {
		return buffer;
	}

	public long getCurrentFileLength() {
		return currentFileLength;
	}

	public String getCurrentFileName() {
		return currentFileName;
	}

	public long getCurrentFileReadLength() {
		return currentFileReadLength;
	}

	public long getCurrentReadLength() {
		return currentFileReadLength;
	}

	public long getCurrentTransportReadSize() {
		return currentTransportReadSize;
	}

	public long getCurrentTransportSize() {
		return currentTransportSize;
	}

	public String getError() {
		return error;
	}

	public String getFileName() {
		// TODO Auto-generated method stub
		if (index < -1 || index > fmps.size() - 1)
			return null;
		return fmps.get(index).getFileName();
	}

	public long getFileNum() {
		return fileNum;
	}

	public long getFileSize() {
		if (index < -1 || index > fmps.size() - 1)
			return 0;
		return fmps.get(index).getFileSize();
	}

	public ArrayList<FileMsgPackage> getFmps() {
		return fmps;
	}

	public int getFmpSize() {
		return fmpSize;
	}

	public int getIndex() {
		return index;
	}

	public InputStream getIs() {
		return is;
	}

	public OutputStream getOs() {
		return os;
	}

	public Socket getSocket() {
		return socket;
	}

	/*
	 * 取得速度，取三次调用的平均值
	 */
	public long getSpeed() {
		long s = 0;
		if (lastCurrentTime == 0) {
			lastCurrentTime = System.currentTimeMillis();
			lastAllReadSize = allReadSize;
		} else {
			long t = System.currentTimeMillis();
			float time = (t - lastCurrentTime) / 1000;
			if (time != 0)
				s = (long) ((allReadSize - lastAllReadSize) / time);
			lastAllReadSize = allReadSize;
			lastCurrentTime = t;
		}
		speedBuffer[0] = speedBuffer[1];
		speedBuffer[1] = speedBuffer[2];
		speedBuffer[2] = s;
		s = (speedBuffer[0] + speedBuffer[1] + speedBuffer[2]) / 3;
		return s;
	}

	public int getStatus() {
		return status;
	}

	public User getUser() {
		return user;
	}

	public String getWorkName() {
		// TODO Auto-generated method stub
		return workName;
	}

	public boolean isCancleNow() {
		return cancleNow;
	}

	protected void setError(String error) {
		System.out.println(error);
		this.error = error;
		status = STATUS_ERROR;
	}

	public void setFmps(ArrayList<FileMsgPackage> fmps) {
		this.fmps = fmps;
	}

	public void setStatus(int status) {
		// TODO Auto-generated method stub
		this.status = status;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
