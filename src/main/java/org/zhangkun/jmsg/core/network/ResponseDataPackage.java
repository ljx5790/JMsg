package org.zhangkun.jmsg.core.network;

import java.util.ArrayList;

import org.zhangkun.jmsg.bean.DataPack;
import org.zhangkun.jmsg.bean.DataPackage;
import org.zhangkun.jmsg.bean.DataUnPack;
import org.zhangkun.jmsg.bean.Msg;
import org.zhangkun.jmsg.bean.User;
import org.zhangkun.jmsg.core.UserManager;
import org.zhangkun.jmsg.core.listener.CancelFileTransportListener;
import org.zhangkun.jmsg.ui.swing.MainWindow;
import org.zhangkun.jmsg.ui.swing.ReceiveFilesDialog;
import org.zhangkun.jmsg.util.GlobalVar;

/**
 * 对数据包进行响应
 * 
 * @author zhangkun
 * 
 */
public class ResponseDataPackage {

	/**
	 * 监听回执消息
	 */
	// private ArrayList<ReportListener> reportListener = new
	// ArrayList<ReportListener>();

	/**
	 * 监听取消文件传送消息
	 */
	private ArrayList<CancelFileTransportListener> cancelFileTransportListener = new ArrayList<CancelFileTransportListener>();

	public ResponseDataPackage() {
	}

	public void addCancelFileTransportListener(CancelFileTransportListener cft) {
		cancelFileTransportListener.add(cft);
	}

	// public void addReportListener(ReportListener rl) {
	// reportListener.add(rl);
	// }
	//
	// public void removeReportListener(ReportListener rl) {
	// reportListener.remove(rl);
	// }

	public void removeCancelFileTransportListener(
			CancelFileTransportListener cft) {
		cancelFileTransportListener.remove(cft);
	}

	public void responce(DataUnPack dup) {
		int model = (int) (dup.getDataType() & 0x000000ff);
		int opt = (int) (dup.getDataType() & 0xffffff00);
		User u = null;
		u = UserManager.instance.getUser(dup);
		User nu = null;
		if (u == null)
			nu = UserManager.instance.createUser(dup);
		User eu = u == null ? nu : u;
		// 该消息为上线
		// System.out.println(type + "登陆" + nu);

		// 回执选项
		if ((opt & DataPackage.IPMSG_SENDCHECKOPT) != 0) {
			DataPack dp = dup.getDataPack();
			dp.setDataType(DataPackage.IPMSG_RECVMSG);
			dp.setMsg(dup.getDataID());
			NetConnection.instance.sendBroadcast(dp);
		}
		DataPack dp = null;
		switch (model) {
		case (int) (DataPackage.IPMSG_SENDMSG):// 文件传送选项
			if ((opt & DataPackage.IPMSG_FILEATTACHOPT) != 0) {
				int i = dup.getMsg().indexOf(".");
				if (i != -1) {
					dup.setMsg(dup.getMsg().substring(i + 1));
					new ReceiveFilesDialog(dup, eu);
					if (dup.getMsg().substring(0, i).length() != 0)
						MainWindow.instance.addMsg(eu, new Msg(dup.getMsg()
								.substring(0, i), Msg.MSG_RECEIVE));
				}
			} else {
				MainWindow.instance.addMsg(eu, new Msg(dup.getMsg(),
						Msg.MSG_RECEIVE));
			}
			break;
		case (int) DataPackage.IPMSG_SENDSHAKE:
			MainWindow.instance.addMsg(eu, new Msg(Msg.MSG_SHAKE));
			break;
		case (int) DataPackage.IPMSG_SEND_STARTINPUT:
			MainWindow.instance.addMsg(eu, new Msg(Msg.MSG_START_INPUT));
			break;
		case (int) DataPackage.IPMSG_SEND_ENDINPUT:
			MainWindow.instance.addMsg(eu, new Msg(Msg.MSG_END_INPUT));
			break;
		case (int) DataPackage.IPMSG_BR_ENTRY:
			dp = dup.getDataPack();
			dp.setDataType(DataPackage.IPMSG_ANSENTRY);
			NetConnection.instance.sendBroadcast(dp);
			setSenderGroupAndName(eu, dup.getMsg());
			break;
		case (int) DataPackage.IPMSG_ANSENTRY:
			// 该消息通报在线
			if (u == null) {
				eu.setName(dup.getMsg());
				setSenderGroupAndName(eu, dup.getMsg());
			}
			break;
		case (int) DataPackage.IPMSG_NOOPERATION:
			return;
		case (int) DataPackage.IPMSG_RECVMSG:
			// 接收回执
			// if (nu != null) {
			// um.addUser(nu);
			// }
			// for (int i = 0; i < reportListener.size(); i++) {
			// if (reportListener.get(i).acceptReport(dup.getMsg()))
			// break;
			// }
			// c.acceptRecvMsg(eu);
			MsgTransport.instance.acceptRecvMsg(dup);
			break;
		case (int) DataPackage.IPMSG_BR_EXIT:
			if (u != null) {
				if (GlobalVar.TRAY_MSG)
					MainWindow.instance.getTray().showExitMsg(u);
			}
			UserManager.instance.delUser(dup);
			return;
		case (int) DataPackage.IPMSG_RELEASEFILES:
			// 终止文件传送
			long id = 0;
			try {
				id = Long.parseLong(dup.getMsg());
				System.out.println(id + " "
						+ cancelFileTransportListener.size());
				for (int i = 0; i < cancelFileTransportListener.size(); i++)
					if (cancelFileTransportListener.get(i)
							.cancelFileTransportListener(id + ""))
						break;
			} catch (Exception e) {
			}
			return;
		}

		if (nu != null) {
			if (!NetConnection.instance.getUsableAddres().contains(nu.getIp())) {
				UserManager.instance.addUser(nu);
				if (GlobalVar.TRAY_MSG)
					MainWindow.instance.getTray().showEntryMsg(nu);
			}
		}
	}

	public void setSenderGroupAndName(User u, String msg) {
		if (msg != null && msg.length() != 0)
			if (msg.indexOf(".") != -1) {
				String[] s = msg.split("\\.");
				if (s[0] != null && s[0].length() > 0
						&& !s[0].equals(u.getName())) {
					u.setName(s[0]);
					MainWindow.instance.repaint();
				}
				if (s.length < 2)
					return;
				u.setGroup(s[1]);
			} else {
				u.setName(msg);
			}
	}
}
