package org.zhangkun.jmsg.core.network.exception;

public class DataPackageParseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8600603660733353935L;

	public DataPackageParseException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DataPackageParseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DataPackageParseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DataPackageParseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
