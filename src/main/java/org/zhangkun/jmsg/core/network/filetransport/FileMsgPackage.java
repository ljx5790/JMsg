package org.zhangkun.jmsg.core.network.filetransport;

import java.io.Serializable;
import java.util.HashMap;

import org.zhangkun.jmsg.bean.DataPackage;
import org.zhangkun.jmsg.core.network.exception.DataPackageParseException;

public class FileMsgPackage extends DataPackage implements
		Comparable<FileMsgPackage>, Serializable {
	public static HashMap<String, String> meFileID = new HashMap<String, String>();
	/**
	 * 
	 */
	private static final long serialVersionUID = 2742428062062419380L;

	public static HashMap<String, String> getMeFileID() {
		return meFileID;
	}

	public static void setMeFileID(HashMap<String, String> meFileID) {
		FileMsgPackage.meFileID = meFileID;
	}

	private boolean enable;
	private int fileID;
	private long fileMktime;
	private int fileMsgType;
	private String fileName;
	private String fileOtherInfo;
	private long fileSize;
	private long offset;

	private String packageID;

	private String path;

	public FileMsgPackage clone() {
		return null;
	}

	@Override
	public int compareTo(FileMsgPackage o) {
		// TODO Auto-generated method stub
		if (this.fileMsgType == 1 && o.getFileMsgType() == 1) {
			return (int) (o.getFileSize() - this.fileSize);
		}
		if (this.fileMsgType == 2 && o.getFileMsgType() == 2) {
			if (this.fileSize == o.getFileSize())
				return this.fileName.compareTo(o.getFileName());
			return (int) (o.getFileSize() - this.fileSize);
		}
		return o.getFileMsgType() - this.fileMsgType;
	}

	public int getFileID() {
		return fileID;
	}

	public long getFileMktime() {
		return fileMktime;
	}

	public int getFileMsgType() {
		return fileMsgType;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileOtherInfo() {
		return fileOtherInfo;
	}

	public long getFileSize() {
		return fileSize;
	}

	public long getOffset() {
		return offset;
	}

	public String getPackageID() {
		return packageID;
	}

	public String getPath() {
		return path;
	}

	public boolean isEnable() {
		return enable;
	}

	public void packFile() {
	}

	public String packToRequest() {
		return Long.toHexString(Long.parseLong(packageID)) + ":"
				+ Integer.toHexString((fileID)) + ":"
				+ Long.toHexString(offset);
	}

	public String packToResponse() {
		return fileID + ":" + fileName + ":" + Long.toHexString(fileSize) + ":"
				+ Long.toHexString(fileMktime) + ":" + fileMsgType + ":";
	}

	public void setEnable(boolean selected) {
		// TODO Auto-generated method stub
		this.enable = selected;
	}

	public void setFileID(int fileID) {
		this.fileID = fileID;
	}

	public void setFileMktime(long fileMktime) {
		this.fileMktime = fileMktime;
	}

	public void setFileMsgType(int fileMsgType) {
		this.fileMsgType = fileMsgType;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileOtherInfo(String fileOtherInfo) {
		this.fileOtherInfo = fileOtherInfo;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void unpackToRequestMsg(String msg) throws DataPackageParseException {
		String[] fileInfo = msg.split(":");
		try {
			packageID = Long.parseLong(fileInfo[0], 16) + "";
			fileID = Integer.parseInt(fileInfo[1]);
			if (fileInfo.length > 2)
				offset = Integer.parseInt(fileInfo[2]);
		} catch (Exception e) {
			throw new DataPackageParseException();
		}
	}

	public void unpackToResponseMsg(String msg) {
		String[] fileInfo = msg.split(":");
		fileID = Integer.parseInt(fileInfo[0]);
		fileName = fileInfo[1];
		fileSize = Long.parseLong(fileInfo[2], 16);
		fileMktime = Long.parseLong(fileInfo[3], 16);
		fileMsgType = Integer.parseInt(fileInfo[4]);
		if (fileInfo.length > 5)
			fileOtherInfo = fileInfo[5];
	}

}
