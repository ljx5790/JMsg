import javax.swing.UIManager;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;
import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.FrameBorderStyle;
import org.zhangkun.jmsg.ui.swing.MainWindow;
import org.zhangkun.jmsg.util.FileUtil;

import java.io.File;

public class TestMain {

    public static void main(String[] args) {
        // if (System.getProperty("os.name").toLowerCase().indexOf("linux") !=
        // -1)
        // System.setProperty("org.eclipse.swt.browser.DefaultType", "webkit");
        // FileUtil.initLib();
        try {
            System.out.println(System.getProperty("os.family") + " " + System.getProperty("os.name") + " " + System.getProperty("os.arch"));
            if (System.getProperty("os.name").toLowerCase().contains("windows")) {
                File xulRunner = new File("xulrunner");
                if (xulRunner.isDirectory() == false)
                    xulRunner = new File("../xulrunner");
                if (xulRunner.isDirectory() == false) {
                    System.err.println("Your system is windows, this program require xulrunner to the location or parent location.");
                    return;
                }
                System.out.println("Set xulrunner path to " + xulRunner.getCanonicalPath());
                System.setProperty("org.eclipse.swt.browser.XULRunnerPath", xulRunner.getCanonicalPath());
            }
            UIManager.put("RootPane.setupButtonVisible", false);
            UIManager.put("ScrollBar.width", new Integer(10));
//            BeautyEyeLNFHelper.frameBorderStyle = FrameBorderStyle.generalNoTranslucencyShadow;
//            org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
//        UIUtil.initTheme();
        MainWindow.instance.init();
    }
}