import java.awt.Component;
import java.util.Enumeration;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

/**
 * 
 * @author zhangkun
 * 
 */
public class UIUtil {


	public static void enable(Component[] cs, boolean enable) {
		if (cs == null)
			return;
		for (int i = 0; i < cs.length; i++) {
			cs[i].setEnabled(enable);
			if (cs[i] instanceof JComponent)
				enable((JComponent) cs[i], enable);
		}
	}

	public static void enable(JComponent parent, boolean enable) {
		parent.setEnabled(enable);
		enable(parent.getComponents(), enable);
	}

	public static void FitTableColumns(JTable myTable) {
		JTableHeader header = myTable.getTableHeader();
		int rowCount = myTable.getRowCount();
		Enumeration columns = myTable.getColumnModel().getColumns();
		while (columns.hasMoreElements()) {
			TableColumn column = (TableColumn) columns.nextElement();
			int col = header.getColumnModel().getColumnIndex(
					column.getIdentifier());
			// 取得表头宽度,然后和内容宽度进行逐一比较,取最大者,如果不想表格宽度随表头变化的话就将下面的width设为0
			int width = (int) myTable
					.getTableHeader()
					.getDefaultRenderer()
					.getTableCellRendererComponent(myTable,
							column.getIdentifier(), false, false, -1, col)
					.getPreferredSize().getWidth();
			for (int row = 0; row < rowCount; row++) {
				int preferedWidth = (int) myTable
						.getCellRenderer(row, col)
						.getTableCellRendererComponent(myTable,
								myTable.getValueAt(row, col), false, false,
								row, col).getPreferredSize().getWidth();
				width = Math.max(width, preferedWidth);
			}
			header.setResizingColumn(column); // 此行很重要
			column.setWidth(width + 20 + myTable.getIntercellSpacing().width);
		}
	}

	public static void initTheme() {
//		JFrame.setDefaultLookAndFeelDecorated(true);
//		JDialog.setDefaultLookAndFeelDecorated(true);
//		updateLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	}

	public static void updateLookAndFeel(JFrame frame) {
		SwingUtilities.updateComponentTreeUI(frame);
	}

	public static void updateLookAndFeel(LookAndFeel lnfName) {
		try {
			UIManager.setLookAndFeel(lnfName);
		} catch (UnsupportedLookAndFeelException ex1) {
			System.err.println("Unsupported LookAndFeel: " + lnfName);
		}
	}

	public static void updateLookAndFeel(String lnfName) {
		try {
			UIManager.setLookAndFeel(lnfName);
		} catch (UnsupportedLookAndFeelException ex1) {
			System.err.println("Unsupported LookAndFeel: " + lnfName);
		} catch (ClassNotFoundException ex2) {
			System.err.println("LookAndFeel class not found: " + lnfName);
		} catch (InstantiationException ex3) {
			System.err.println("Could not load LookAndFeel: " + lnfName);
		} catch (IllegalAccessException ex4) {
			System.err.println("Cannot use LookAndFeel: " + lnfName);
		}
	}
}
