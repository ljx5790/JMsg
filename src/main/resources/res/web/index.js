var msgHTML;
var msgObject;
var inputObject;
var allDivObject;
var editor;
var meName;
var meIP;
var meGroup;
var editor;
var useHTML = true;
var lastText;
var delay;
var hasFocus = false;

function updateInputStatus() {
	var currText = editor.getSource();
	if (currText == "") {
		delay = 0;
		if (hasFocus)
			document.title = "endInputEvent";
		hasFocus = false;
	} else {
		if (lastText == currText) {
			if (delay < 10)
				delay++;
			else {
				delay = 0;
				if (hasFocus)
					document.title = "endInputEvent";
				hasFocus = false;
			}
		} else {
			if (!hasFocus)
				document.title = "startInputEvent";
			hasFocus = true;
		}
	}
	lastText = currText;
}

$(document).ready(function() {
	msgHTML = "";
	msgObject = document.getElementById("msgDiv");
	inputObject = document.getElementById("inputDiv");
	allDivObject = document.getElementById("allDiv");
	setInterval("updateInputStatus()", 500);
});
function resizeEvent() {
	createEditor();
};
var allPlugin = {
	Code : {
		c : 'btnCode',
		t : '插入代码',
		h : 1,
		e : function() {
			var _this = this;
			var htmlCode = '<div><select id="xheCodeType"><option value="html">XML/HTML</option><option value="js">Javascript</option><option value="css">CSS</option><option value="php">PHP</option><option value="java">Java</option><option value="py">Python</option><option value="pl">Perl</option><option value="rb">Ruby</option><option value="cs">C#</option><option value="c">C++/C</option><option value="vb">VB/ASP</option><option value="">其它</option></select></div><div><textarea id="xheCodeValue" wrap="soft" spellcheck="false" style="width:300px;height:200px;" /></div><div style="text-align:right;"><input type="button" id="xheSave" value="确定" /></div>';
			var jCode = $(htmlCode), jType = $('#xheCodeType', jCode), jValue = $(
					'#xheCodeValue', jCode), jSave = $('#xheSave', jCode);
			jSave
					.click(function() {
						_this.loadBookmark();
						_this
								.pasteHTML(
										'<div class="codeDiv"><div class="codeBtn"><a href="#">折叠代码</a></div><pre class="brush:'
												+ jType.val()
												+ ';">'
												+ _this.domEncode(jValue.val())
												+ '</pre></div><p></p>', false);
						_this.hidePanel();
						return false;
					});
			_this.showDialog(jCode);
		}
	},
	MyEmot : {
		c : 'myEmotBtn',
		t : '表情',
		h : 1,
		e : function() {
			var _this = this;
			var htmlCode = loadMyEmotDiv();
			var jCode = $(htmlCode);
			// alert(htmlCode);
			_this.showDialog(jCode);
			changeMyEmot(lastOpenEmotImg);
		}

	},
	InsertIMG : {
		c : 'insertImgBtn',
		t : '插入图片',
		e : function() {
			sendImg();
		}
	},
	Shake : {
		c : 'shakeBtn',
		t : '闪屏',
		e : function() {
			shakeWindow();
		}
	},
	ScreenCapture : {
		c : 'screenCaptureBtn',
		t : '截屏',
		e : function() {
			screenCapture();
		}
	},
	SendMe : {
		c : 'sendMeBtn',
		t : '发送程序',
		e : function() {
			showNoHTMLMsg();
		}
	}
};

var myEmotPackages = new Array(new Array("QQ", "qq"), new Array("奶瓶仔", "npz"));
var myEmots = new Array(28, 71);
var lastOpenEmotImg = myEmotPackages[0][1];

function changeMyEmot(str) {
	$(".emotTitleLi").removeClass("selEmotTitleLi");
	$("#MyEmod_title_" + str).addClass("selEmotTitleLi");
	$(".MyEmotImgDiv").hide();
	$("#MyEmot_Img_Div_" + str).show();
	lastOpenEmotImg = str;
}

function changePreviewDivPosition() {
	var emod_preview_div = $("#emod_preview_div");
	var left = emod_preview_div.css("left");
	if (left == "0px" || left == "auto") {
		emod_preview_div.css("left", 264);
	} else {
		emod_preview_div.css("left", 0);
	}
}

function changeMyEmotImg(id, type, imgSrc) {
	var obj = $("#" + id);
	var emod_preview_div = $("#emod_preview_div");
	if (type == 2) {
		obj.removeClass("selMyEmotImg");
		obj.addClass("myEmotTd");
		emod_preview_div.hide();
	} else {
		obj.removeClass("myEmotTd");
		obj.addClass("selMyEmotImg");
		emod_preview_div.html('<center><img src="' + imgSrc
				+ '" style="max-width:100%;max-height:100%;" /></center>');
		emod_preview_div.show();
	}
}

function insertMyEmot(imgSrc) {
	insertHTML('<img src="' + imgSrc + '"/>');
	editor.hidePanel();
}

function loadMyEmotDiv() {
	var htmlStr = "";
	htmlStr += '<div id="MyEmot" style="width:315px;height:250px;">';
	htmlStr += '<div style="width:335px;position: relative;left:-10px;top:-5px;height:20px;border-bottom:1px solid gray;"><ul style="margin-left:10px;">';
	var i = 0;
	for (i = 0; i < myEmotPackages.length; i++) {
		htmlStr += '<li class="emotTitleLi" id="MyEmod_title_'
				+ myEmotPackages[i][1] + '" onmousedown="changeMyEmot(\''
				+ myEmotPackages[i][1] + '\')"> ' + myEmotPackages[i][0]
				+ "</li>";
	}
	htmlStr += '</ul></div><div id="emod_preview_div" onMouseOver="changePreviewDivPosition()"></div>';
	for (i = 0; i < myEmots.length; i++) {
		var j = 0;
		htmlStr += '<div id="MyEmot_Img_Div_'
				+ myEmotPackages[i][1]
				+ '" style="display:none;" class="MyEmotImgDiv"><table cellpadding="0" cellspacing="0">';
		for (j = 1; j <= myEmots[i]; j++) {
			if (j % 9 == 1)
				htmlStr += "\n<tr>\n";
			// htmlStr += " <td></td>";

			htmlStr += '<td align="center" class="myEmotTd"  id="MyEmot_Img_Div_'
					+ myEmotPackages[i][1]
					+ '_'
					+ j
					+ '" onMouseOver="changeMyEmotImg(\'MyEmot_Img_Div_'
					+ myEmotPackages[i][1]
					+ '_'
					+ j
					+ '\',1,\'emot/'
					+ myEmotPackages[i][1]
					+ '/'
					+ j
					+ '.gif\')"  onMouseOut="changeMyEmotImg(\'MyEmot_Img_Div_'
					+ myEmotPackages[i][1]
					+ '_'
					+ j
					+ '\',2,\'emot/'
					+ myEmotPackages[i][1]
					+ '/'
					+ j
					+ '.gif\')"  onmousedown="insertMyEmot(\'emot/'
					+ myEmotPackages[i][1]
					+ '/'
					+ j
					+ '.gif\')" ><img src="emot/'
					+ myEmotPackages[i][1]
					+ '/'
					+ j
					+ '.gif" style="max-width:25px;max-height:25px;"/></td>';
			if (j % 9 == 0)
				htmlStr += "\n</tr>\n";
			if (j == myEmots[i] && j % 9 != 0) {
				var z = 0;
				for (z = 1; z <= 9 - j % 9; z++)
					htmlStr += "<td></td>";
				htmlStr += "\n</tr>\n";
			}
		}
		htmlStr += '</table></div>';
	}
	htmlStr += '</div>';
	return htmlStr;
}

function shakeWindow() {
	document.title = "shakeWindowEvent";
}

function screenCapture() {
	document.title = "screenCaptureEvent";
}

function createEditor() {
	var par;
	var height = $("#inputDiv").height();
	if (useHTML)
		par = {
			tools : 'MyEmot,InsertIMG,Shake,ScreenCapture,Pastetext,|,Fontface,FontSize,Bold,Italic,Underline,Strikethrough,Removeformat,FontColor,BackColor,Align,Hr,|,Code,Table,Link,Unlink,Fullscreen',
			forcePtag : false,
			emotMark : true,
			emots : {
				npz : {
					name : '奶瓶仔',
					count : 24,
					width : 80,
					height : 80,
					line : 5
				},
				msn : {
					name : 'MSN',
					count : 40,
					width : 22,
					height : 22,
					line : 8
				}
			},
			plugins : allPlugin,
			height : height,
			inlineScript : true,
			internalStyle : true,
			width : $("#inputDiv").width()
		};
	else
		par = {
			tools : 'SendMe,Fullscreen',
			plugins : allPlugin,
			cleanPaste : 1,
			height : height,
			width : $("#inputDiv").width()
		};
	document.title = "";
	if (editor) {
		$('#inputArea').xheditor(false);
		editor = null;
	}
	editor = $('#inputArea').xheditor(par);
	editor.focus();
	editor.addShortcuts('ctrl+enter', function() {
		document.title = "sendMsgEvent";
	});
	if (!useHTML) {
		editor.toggleSource(true);
	} else {
		editor.toggleSource(false);
	}
}

function clearTitle() {
	document.title = "";
}

function sendImg() {
	document.title = "sendImgEvent";
}
var sendMeMsg = "我正在使用JMsg，可以发送图片表情，还可以编辑字体颜色。功能很强大哦～～体验一下吧。";
function sendJMsg(obj) {
	sendMeMsg = $(".sendMe").val();
	document.title = "sendJMsgEvent";
	editor.removeModal();
}

function getSendMeMsg() {
	return sendMeMsg;
}

function showNoHTMLMsg() {
	editor
			.showModal(
					"提示：",
					"<p style=\"padding:3px;font-size:15px;\">对方使用的不是Java版飞鸽，所以您只能向其发送普通文字。不能发送图片和表情，也无法发送带颜色的文字等。是否向其发送Java版飞鸽程序以便获得更好体验？</a><hr/><br/><center><textarea class=\"sendMe\" style=\"width:280px;height:100px;\">"
							+ sendMeMsg
							+ "</textarea><br/><input type=\"button\" style=\"width:90%;margin-top:5px;\" onclick=\"javascript:sendJMsg(this)\" value=\"发送\"/></center><br/>",
					300, 250, function() {
					});
}

function updateUseHtml(use) {
	useHTML = use;
	if (!useHTML) {
		if (editor)
			editor.toggleSource(true);
	} else {
		if (editor)
			editor.toggleSource(false);
	}
}

function updateMeInfo(name, ip, group) {
	meName = name;
	meIP = ip;
	meGroup = group;
}

function appendReceiveMsg(name, ip, group, msgStr) {
	// if (!useHTML)
	// msgStr = html_encode(msgStr);
	msgHTML += "<div class=\"receiveDiv\"><div class=\"receiveTitleDiv\"  onmouseover=\"toggleUserInfo(this,true);\" onmouseout=\"toggleUserInfo(this,false);\">";
	msgHTML += "<label class=\"receiveName\">" + name + "：</label>";
	msgHTML += "<div class=\"otherUserInfo\"><label class=\"receiveDate\">"
			+ getDate() + "</label>";
	msgHTML += "<label class=\"receiveIP\">" + ip + "</label>";
	msgHTML += "<label class=\"receiveGroup\">[" + group + "]</label></div>";
	msgHTML += "</div><div class=\"receiveMsgDiv\">";
	msgHTML += msgStr;
	msgHTML += "</div></div>";
	msgObject.innerHTML = msgHTML;
	scrollToBottom();
}

function appendSystemMsg(msgStr) {

	msgHTML += "<div class=\"systemDiv\"><div class=\"systemTitleDiv\"  onmouseover=\"toggleUserInfo(this,true);\" onmouseout=\"toggleUserInfo(this,false);\">";
	msgHTML += "<label class=\"systemName\"> 系统提示：</label>";
	msgHTML += "<div class=\"otherUserInfo\"><label class=\"receiveDate\">"
			+ getDate() + "</label></div>";
	msgHTML += "</div><div class=\"systemMsgDiv\">";
	msgHTML += msgStr;
	msgHTML += "</div></div>";
	msgObject.innerHTML = msgHTML;

	scrollToBottom();
}

function appendSendMsg(msgStr) {
	// if (!useHTML)
	// msgStr = html_encode(msgStr);
	msgHTML += "<div class=\"meDiv\"><div class=\"meTitleDiv\"   onmouseover=\"toggleUserInfo(this,true);\" onmouseout=\"toggleUserInfo(this,false);\">";
	msgHTML += "<label class=\"meName\">" + meName + "：</label>";
	msgHTML += "<div class=\"otherUserInfo\"><label class=\"meDate\">"
			+ getDate() + "</label>";
	msgHTML += "<label class=\"meIP\">" + meIP + "</label>";
	msgHTML += "<label class=\"meGroup\">[" + meGroup + "]</label></div>";
	msgHTML += "</div><div class=\"meMsgDiv\">";
	msgHTML += msgStr;
	msgHTML += "</div></div>";
	msgObject.innerHTML = msgHTML;
	scrollToBottom();
}

function toggleUserInfo(obj, flag) {
	obj = obj.childNodes[1];
	if (flag) {
		obj.style.display = "inline";
	} else {
		obj.style.display = "none";
	}
}

function scrollToBottom() {
	SyntaxHighlighter.highlight();
	msgObject.scrollTop = msgObject.scrollHeight;
	updateFocus();
}

function countSize() {
	var width = document.body.clientWidth;
	var height = document.body.clientHeight;
	allDivObject.style.width = width;
	allDivObject.style.height = height;
	allDivObject.style.left = 0;
	allDivObject.style.top = 0;
}

function checkTime(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i
}

function getDate() {
	var today = new Date();
	var h = today.getHours();
	var m = today.getMinutes();
	var s = today.getSeconds();
	m = checkTime(m);
	s = checkTime(s);
	var now = "";
	now = today.getFullYear() + "-";
	now = now + (today.getMonth() + 1) + "-";
	now = now + today.getDate() + " ";
	now = now + h + ":";
	now = now + m + ":";
	now = now + s + "";
	return now;
}

function send() {
	var text = editor.getSource();
	if (text != "") {
		appendSendMsg(text);
		editor.setSource("");
	}
	scrollToBottom();
}

function getInputText() {
	return editor.getSource();
}

function setInputText(text) {
	editor.setSource(text);
}

function insertHTML(str) {
	editor.pasteHTML(str, false);
}

function updateFocus() {
	editor.focus();
}

function saveImg(fid, ip) {
	document.title = "saveImgEvent#" + fid + "#" + ip;
}

function openImg(fid, ip) {
	document.title = "openImgEvent#" + fid + "#" + ip;
}

function html_encode(str) {
	var s = "";
	if (str.length == 0)
		return "";
	s = str.replace(/&/g, "&gt;");
	s = s.replace(/</g, "&lt;");
	s = s.replace(/>/g, "&gt;");
	s = s.replace(/ /g, "&nbsp;");
	s = s.replace(/\'/g, "&#39;");
	s = s.replace(/\"/g, "&quot;");
	s = s.replace(/\n/g, "<br>");
	return s;
}

function html_decode(str) {
	var s = "";
	if (str.length == 0)
		return "";
	s = str.replace(/&gt;/g, "&");
	s = s.replace(/&lt;/g, "<");
	s = s.replace(/&gt;/g, ">");
	s = s.replace(/&nbsp;/g, " ");
	s = s.replace(/&#39;/g, "\'");
	s = s.replace(/&quot;/g, "\"");
	s = s.replace(/<br>/g, "\n");
	return s;
}
